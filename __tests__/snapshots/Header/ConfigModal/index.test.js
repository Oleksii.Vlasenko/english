import React from "react";
import {Provider} from "react-redux";
import {compose} from "redux";
import {shallow, configure} from 'enzyme';
import toJson from "enzyme-to-json";
import Adapter from 'enzyme-adapter-react-16';
import configureStore from "../../../../src/store";
import ConfigureModal from "../../../../src/components/Header/ConfigModal";

configure({ adapter: new Adapter() });

describe("<ConfigureModal /> UI Component", () => {
    const shallowExpect = compose(expect, toJson, shallow);
    let show = false;
    let store;
    beforeAll(() => {
        store = configureStore({});
        show = false;
    });
    it('Renders correct properties', () => {
        shallowExpect(
            <Provider store={store}>
                <ConfigureModal show={show}
                                onHide={() => show = false}
                />
            </Provider>
        ).toMatchSnapshot();
    });
});
