import C from "../../../src/store/consts/index";
import vocabularyTasks from "../../../src/store/reducers/vocabularyTasks";

describe("VOCABULARY_TASKS reducer", () => {
    it("LOADING successes", () => {
        const state = {
            vocabularyTasksList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_VOCABULARYTASK
        };
        const result = vocabularyTasks(state, action);
        expect(result)
            .toEqual({
                vocabularyTasksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {vocabularyTasksList: [], isLoading: true};
        const action = {
            type: C.SAVE_VOCABULARYTASKS,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = vocabularyTasks(state, action);
        expect(result)
            .toEqual({
                vocabularyTasksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("CREATE successes", () => {
        const state = {vocabularyTasksList: [{title: "First"}], isLoading: true};
        const action = {
            type: C.CREATE_VOCABULARYTASK,
            store: {title: "Second"}
        };
        const result = vocabularyTasks(state, action);
        expect(result)
            .toEqual({
                vocabularyTasksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("EDIT successes", () => {
        const state = {
            vocabularyTasksList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.EDIT_VOCABULARYTASK,
            store: {_id: "111", title: "Third"}
        };
        const result = vocabularyTasks(state, action);
        expect(result)
            .toEqual({
                vocabularyTasksList: [
                    {_id: "111", title: "Third"},
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
    it("REMOVE successes", () => {
        const state = {
            vocabularyTasksList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.REMOVE_VOCABULARYTASK,
            store: {_id: "111", title: "First"}
        };
        const result = vocabularyTasks(state, action);
        expect(result)
            .toEqual({
                vocabularyTasksList: [
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
});
