import C from "../../../src/store/consts/index";
import marks from "../../../src/store/reducers/marks";

describe("MARKS reducer", () => {
    it("LOADING successes", () => {
        const state = {
            marksList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_MARK
        };
        const result = marks(state, action);
        expect(result)
            .toEqual({
                marksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {marksList: [], isLoading: true};
        const action = {
            type: C.SAVE_MARKS_BY_TASKGROUP,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = marks(state, action);
        expect(result)
            .toEqual({
                marksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("CREATE successes", () => {
        const state = {marksList: [{title: "First"}, {title: "Second"}], isLoading: true};
        const action = {
            type: C.CREATE_MARK,
            store: {title: "Third"}
        };
        const result = marks(state, action);
        expect(result)
            .toEqual({
                marksList: [
                    {title: "First"},
                    {title: "Second"},
                    {title: "Third"}
                ],
                isLoading: false
            });
    });
    it("EDIT successes", () => {
        const state = {
            marksList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.EDIT_MARK,
            store: {_id: "111", title: "Third"}
        };
        const result = marks(state, action);
        expect(result)
            .toEqual({
                marksList: [
                    {_id: "222", title: "Second"},
                    {_id: "111", title: "Third"}
                ],
                isLoading: false
            });
    });
    it("REMOVE successes", () => {
        const state = {
            marksList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.REMOVE_MARK,
            store: {_id: "111", title: "First"}
        };
        const result = marks(state, action);
        expect(result)
            .toEqual({
                marksList: [
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
});
