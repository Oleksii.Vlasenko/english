import C from "../../../src/store/consts/index";
import grammarThemes from "../../../src/store/reducers/grammarThemes";

describe("GRAMMAR_THEMES reducer", () => {
    it("SAVE successes", () => {
        const state = [];
        const action = {
            type: C.SAVE_THEMES,
            store: [
                {title: "First"},
                {title: "Second"}
            ]
        };
        const result = grammarThemes(state, action);
        expect(result)
            .toEqual([
                    {title: "First"},
                    {title: "Second"}
                ]);
    });
});
