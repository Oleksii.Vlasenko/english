import C from "../../../src/store/consts/index";
import vocabularyThemes from "../../../src/store/reducers/vocabularyThemes";

describe("VOCABULARY_THEMES reducer", () => {
    it("LOADING successes", () => {
        const state = {
            vocabularyThemesList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_VOCABULARYTHEMES
        };
        const result = vocabularyThemes(state, action);
        expect(result)
            .toEqual({
                vocabularyThemesList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {vocabularyThemesList: [], isLoading: true};
        const action = {
            type: C.SAVE_VOCABULARYTHEMES,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = vocabularyThemes(state, action);
        expect(result)
            .toEqual({
                vocabularyThemesList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
});
