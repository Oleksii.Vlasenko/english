import C from "../../../src/store/consts/index";
import auth from "../../../src/store/reducers/auth/auth";

describe("AUTH reducer", () => {
    it("LOGIN successes", () => {
        const state = {};
        const action = {
            type: C.LOGIN,
            store: {
                token: "12345678",
                firstName: "Name",
                lastName: "Lastname",
                role: "teacher",
                id: "12345678"
            }
        };
        const result = auth(state, action);
        expect(result)
            .toEqual({
                isLoading: false,
                user: {
                    token: "12345678",
                    firstName: "Name",
                    lastName: "Lastname",
                    role: "teacher",
                    id: "12345678"
                }
            });
    });
    it("LOGOUT successes", () => {
        const state = {
            isLoading: false,
            user: {
                token: "12345678",
                firstName: "Name",
                lastName: "Lastname",
                role: "teacher",
                id: "12345678"
            }
        };
        const action = {
            type: C.LOGOUT
        };
        const result = auth(state, action);
        expect(result)
            .toEqual({
                isLoading: false,
                user: {}
            });
    });
    it("EDIT_USER successes", () => {
        const state = {
            isLoading: false,
            user: {
                token: "12345678",
                firstName: "Name",
                lastName: "Lastname",
                role: "teacher",
                id: "12345678"
            }
        };
        const action = {
            type: C.EDIT_USER,
            store: {
                firstName: "Firstname",
                lastName: "Surname"
            }
        };
        const result = auth(state, action);
        expect(result)
            .toEqual({
                isLoading: false,
                user: {
                    token: "12345678",
                    firstName: "Firstname",
                    lastName: "Surname",
                    role: "teacher",
                    id: "12345678"
                }
            });
    });
    it("LOADING successes", () => {
        const state = {
            isLoading: false,
            user: {
                token: "12345678",
                firstName: "Firstname",
                lastName: "Surname",
                role: "teacher",
                id: "12345678"
            }
        };
        const result = auth(state, {type: C.LOADING_USER});
        expect(result)
            .toEqual({
                isLoading: true,
                user: {
                    token: "12345678",
                    firstName: "Firstname",
                    lastName: "Surname",
                    role: "teacher",
                    id: "12345678"
                }
            });
    });
});

