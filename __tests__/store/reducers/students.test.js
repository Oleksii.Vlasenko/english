import C from "../../../src/store/consts/index";
import students from "../../../src/store/reducers/students";

describe("STUDENTS reducer", () => {
    it("LOADING successes", () => {
        const state = {
            studentsList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_STUDENT
        };
        const result = students(state, action);
        expect(result)
            .toEqual({
                studentsList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {studentsList: [], isLoading: true};
        const action = {
            type: C.GET_STUDENTS,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = students(state, action);
        expect(result)
            .toEqual({
                studentsList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("EDIT successes", () => {
        const state = {
            studentsList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.EDIT_STUDENT,
            store: {_id: "111", title: "Third"}
        };
        const result = students(state, action);
        expect(result)
            .toEqual({
                studentsList: [
                    {_id: "111", title: "Third"},
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
    it("REMOVE successes", () => {
        const state = {
            studentsList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.REMOVE_STUDENT,
            store: {_id: "111", title: "First"}
        };
        const result = students(state, action);
        expect(result)
            .toEqual({
                studentsList: [
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
});
