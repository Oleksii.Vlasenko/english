import C from "../../../src/store/consts/index";
import grammarSubthemes from "../../../src/store/reducers/grammarSubthemes/index";

describe("GRAMMAR_SUBTHEMES reducer", () => {
    it("SAVE successes", () => {
        const state = [];
        const action = {
            type: C.SAVE_SUBTHEMES,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = grammarSubthemes(state, action);
        expect(result)
            .toEqual([{title: "First"}, {title: "Second"}]);
    });
});
