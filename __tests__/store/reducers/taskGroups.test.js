import C from "../../../src/store/consts/index";
import taskGroups from "../../../src/store/reducers/taskGroups";

describe("TASKSGROUPS reducer", () => {
    it("LOADING successes", () => {
        const state = {
            taskGroupsList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_TASKGROUP
        };
        const result = taskGroups(state, action);
        expect(result)
            .toEqual({
                taskGroupsList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {taskGroupsList: [], isLoading: true};
        const action = {
            type: C.SAVE_TASKGROUPS,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = taskGroups(state, action);
        expect(result)
            .toEqual({
                taskGroupsList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("SAVE_BY_ID successes", () => {
        const state = {taskGroupsList: [], isLoading: true};
        const action = {
            type: C.SAVE_TASKGROUPS,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = taskGroups(state, action);
        expect(result)
            .toEqual({
                taskGroupsList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("EDIT successes", () => {
        const state = {
            taskGroupsList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.EDIT_TASKGROUP,
            store: {_id: "111", title: "Third"}
        };
        const result = taskGroups(state, action);
        expect(result)
            .toEqual({
                taskGroupsList: [
                    {_id: "111", title: "Third"},
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
    it("REMOVE successes", () => {
        const state = {
            taskGroupsList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.REMOVE_TASKGROUP,
            store: {_id: "111", title: "First"}
        };
        const result = taskGroups(state, action);
        expect(result)
            .toEqual({
                taskGroupsList: [
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
});
