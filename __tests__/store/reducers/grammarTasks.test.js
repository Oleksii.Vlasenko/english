import C from "../../../src/store/consts/index";
import grammarTasks from "../../../src/store/reducers/grammarTasks/index";

describe("GRAMMAR_TASKS reducer", () => {
    it("LOADING successes", () => {
        const state = {
            grammarTasksList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_GRAMMARTASK
        };
        const result = grammarTasks(state, action);
        expect(result)
            .toEqual({
                grammarTasksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {grammarTasksList: [], isLoading: true};
        const action = {
            type: C.SAVE_GRAMMARTASKS,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = grammarTasks(state, action);
        expect(result)
            .toEqual({
                grammarTasksList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("CREATE successes", () => {
        const state = {grammarTasksList: [{title: "First"}, {title: "Second"}], isLoading: true};
        const action = {
            type: C.CREATE_GRAMMARTASK,
            store: {title: "Third"}
        };
        const result = grammarTasks(state, action);
        expect(result)
            .toEqual({
                grammarTasksList: [
                    {title: "First"},
                    {title: "Second"},
                    {title: "Third"}
                ],
                isLoading: false
            });
    });
    it("EDIT successes", () => {
        const state = {
            grammarTasksList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.EDIT_GRAMMARTASK,
            store: {_id: "111", title: "Third"}
        };
        const result = grammarTasks(state, action);
        expect(result)
            .toEqual({
                grammarTasksList: [
                    {_id: "111", title: "Third"},
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
    it("REMOVE successes", () => {
        const state = {
            grammarTasksList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.REMOVE_GRAMMARTASK,
            store: {_id: "111", title: "First"}
        };
        const result = grammarTasks(state, action);
        expect(result)
            .toEqual({
                grammarTasksList: [
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
});
