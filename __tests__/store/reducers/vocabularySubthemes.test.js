import C from "../../../src/store/consts/index";
import vocabularySubthemes from "../../../src/store/reducers/vocabularySubthemes";

describe("VOCABULARY_SUBTHEMES reducer", () => {
    it("LOADING successes", () => {
        const state = {
            vocabularySubthemesList: [
                {title: "First"},
                {title: "Second"}
            ],
            isLoading: false
        };
        const action = {
            type: C.LOADING_VOCABULARYSUBTHEMES
        };
        const result = vocabularySubthemes(state, action);
        expect(result)
            .toEqual({
                vocabularySubthemesList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {vocabularySubthemesList: [], isLoading: true};
        const action = {
            type: C.SAVE_VOCABULARYSUBTHEMES,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = vocabularySubthemes(state, action);
        expect(result)
            .toEqual({
                vocabularySubthemesList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
});
