import C from "../../../src/store/consts/index";
import groups from "../../../src/store/reducers/groups";

describe("GROUPS reducer", () => {
    it("LOADING successes", () => {
        const state = {
            groupsList: [],
            isLoading: false
        };
        const action = {
            type: C.LOADING_GROUP
        };
        const result = groups(state, action);
        expect(result)
            .toEqual({
                groupsList: [],
                isLoading: true
            });
    });
    it("SAVE successes", () => {
        const state = {groupsList: [], isLoading: true};
        const action = {
            type: C.SAVE_GROUPS,
            store: [{title: "First"}, {title: "Second"}]
        };
        const result = groups(state, action);
        expect(result)
            .toEqual({
                groupsList: [
                    {title: "First"},
                    {title: "Second"}
                ],
                isLoading: false
            });
    });
    it("CREATE successes", () => {
        const state = {groupsList: [{title: "First"}, {title: "Second"}], isLoading: true};
        const action = {
            type: C.CREATE_GROUP,
            store: {title: "Third"}
        };
        const result = groups(state, action);
        expect(result)
            .toEqual({
                groupsList: [
                    {title: "First"},
                    {title: "Second"},
                    {title: "Third"}
                ],
                isLoading: false
            });
    });
    it("EDIT successes", () => {
        const state = {
            groupsList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.EDIT_GROUP,
            store: {_id: "111", title: "Third"}
        };
        const result = groups(state, action);
        expect(result)
            .toEqual({
                groupsList: [
                    {_id: "111", title: "Third"},
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
    it("REMOVE successes", () => {
        const state = {
            groupsList: [
                {_id: "111", title: "First"},
                {_id: "222", title: "Second"}
            ],
            isLoading: true
        };
        const action = {
            type: C.REMOVE_GROUP,
            store: {_id: "111", title: "First"}
        };
        const result = groups(state, action);
        expect(result)
            .toEqual({
                groupsList: [
                    {_id: "222", title: "Second"}
                ],
                isLoading: false
            });
    });
});
