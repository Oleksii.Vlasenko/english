import configureStore from "../../src/store";
import C from "../../src/store/consts/index";

describe("add store success", () => {
    let action;
    action = {
        type: C.SAVE_SUBTHEMES,
        store: [
            {_id: "gramSubteme_1", title: "theme_1"},
            {_id: "gramSubteme_2", title: "theme_2"}
        ]
    };
    let store;
    beforeAll(() => {
        store = configureStore();
        store.dispatch(action);
    });
    it("should toBeDefined article", () => {
        expect(store.getState().grammarSubthemes[0]._id).toBeDefined();
    });
    it("should toBe price", () => {
        expect(store.getState().grammarSubthemes[0].title).toBe("theme_1");
    });
});
