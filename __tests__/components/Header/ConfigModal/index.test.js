import React from "react";
import {mount, configure} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import MenuHeaderItem from "../../../../src/components/Header/MenuHeader/MenuHeaderItem";

configure({adapter: new Adapter()});

describe("creating MenuHeaderItem", () => {

    let modalBlock;
    const action = jest.fn();

    beforeAll(() => {
        const staffroom = ["Groups", "Tasks", "Marks"];
        modalBlock = mount(
                <MenuHeaderItem title={"test"}
                                list={staffroom}
                                action={action}
                />
        );
    });
    it('should create component with a', () => {
        expect(modalBlock.find('a').length).toEqual(1);
    });
    it('should create component with .dropdown', () => {
        expect(modalBlock.find('.dropdown-toggle').length).toEqual(4);
    });
});
