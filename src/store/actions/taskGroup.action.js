import C from '../consts';
import {
    createTaskGroups,
    getTaskGroups,
    getStudentTaskGroups,
    removeTaskGroup,
    editTaskGroup
} from "../../services/taskGroup.service";

export const saveTaskGroups = () =>
    async (dispatch, token) => {
        dispatch({type: C.LOADING_TASKGROUP});
        await getTaskGroups({token})
            .then(result => {
                dispatch({
                    type: C.SAVE_TASKGROUPS,
                    store: result.data
                })
            })
    };

export const saveStudentTaskGroups = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_TASKGROUP});
        await getStudentTaskGroups({token, id})
            .then(result => {
                dispatch({
                    type: C.SAVE_STUDENTTASKGROUPS,
                    store: result.data
                })
            });
    };

export const addTaskGroup = () =>
    async (dispatch, token, body) => {
        dispatch({type: C.LOADING_TASKGROUP});
        await createTaskGroups({token, body})
            .then(result => {
                dispatch({
                    type: C.CREATE_TASKGROUP,
                    store: result.data
                })
            })
    };

export const editTaskGroupById = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_TASKGROUP});
        await editTaskGroup({token, body, id})
            .then(result => {
                dispatch({
                    type: C.EDIT_TASKGROUP,
                    store: result.data
                })
            })
    };

export const removeTaskGroupById = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_TASKGROUP});
        await removeTaskGroup({token, id})
            .then(result => {
                dispatch({
                    type: C.REMOVE_TASKGROUP,
                    store: result.data
                })
            })
    };
