import C from '../consts';

import {
    getVocabularySubthemes
} from "../../services/vocabularySubthemes.service";

export const saveVocabularySubthemes = () =>
    async (dispatch) => {
        dispatch({type: C.LOADING_VOCABULARYSUBTHEMES});
        await getVocabularySubthemes()
            .then(result => {
                dispatch({
                    type: C.SAVE_VOCABULARYSUBTHEMES,
                    store: result.data
                })
            })
    };
