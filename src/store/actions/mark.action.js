import C from '../consts';
import {
    getMarks,
    getMarksByTaskGroup,
    createMark,
    editMark,
    removeMark
} from "../../services/mark.service";

export const saveMarks = () =>
    async (dispatch, token) => {
        dispatch({type: C.LOADING_MARK});
        await getMarks({token})
            .then(result => {
                dispatch({
                    type: C.SAVE_MARKS,
                    store: result.data
                })
            });
    };

export const saveMarksByTaskGroup = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_MARK});
        await getMarksByTaskGroup({token, id})
            .then(result => {
                dispatch({
                    type: C.SAVE_MARKS_BY_TASKGROUP,
                    store: result.data
                })
            });
    };

export const addMark = () =>
    async (dispatch, token, body) => {
        dispatch({type: C.LOADING_MARK});
        await createMark({token, body})
            .then(result => {
                dispatch({
                    type: C.CREATE_MARK,
                    store: result.data
                })
            });
    };

export const editMarkById = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_MARK});
        await editMark({token, body, id})
            .then(result => {
                dispatch({
                    type: C.EDIT_MARK,
                    store: result.data
                })
            });
    };

export const removeMarkById = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_MARK});
        await removeMark({token, id})
            .then(result => {
                dispatch({
                    type: C.REMOVE_MARK,
                    store: result.data
                })
            });
    };
