import C from '../consts';
import {
    getGroups,
    editGroup,
    createGroup,
    removeGroup
} from "../../services/groups.service";

export const saveGroups = () =>
    async (dispatch, token) => {
        dispatch({type: C.LOADING_GROUP});
        await getGroups(token)
            .then(result => {
                dispatch({
                    type: C.SAVE_GROUPS,
                    store: result.data
                })
            });
    };

export const editGroupById = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_GROUP});
        await editGroup({id, body, token})
            .then(result => {
                dispatch({
                    type: C.EDIT_GROUP,
                    store: result.data
                })
            })
    };

export const createNewGroup = () =>
    async (dispatch, token, body) => {
        dispatch({type: C.LOADING_GROUP});
        await createGroup({body, token})
            .then(result => {
                dispatch({
                        type: C.CREATE_GROUP,
                        store: result.data
                    })
            });
    };

export const removeGroupById = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_GROUP});
        await removeGroup({token, id})
            .then(result => {
                dispatch({
                    type: C.REMOVE_GROUP,
                    store: result.data
                })
            })
    };
