import C from '../consts';
import {
    getVocabularyTasks,
    createVocabularyTasks,
    editVocabularyTask,
    removeVocabularyTask
} from "../../services/vocabularyTasks.service";

export const saveVocabularyTasks = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_VOCABULARYTASK});
        await getVocabularyTasks({token, id})
            .then(result => {
                dispatch({
                    type: C.SAVE_VOCABULARYTASKS,
                    store: result.data
                })
            });
    };

export const addVocabularyTask = () =>
    async (dispatch, token, body) => {
        dispatch({type: C.LOADING_VOCABULARYTASK});
        await createVocabularyTasks({token, body})
            .then(result => {
                dispatch({
                    type: C.CREATE_VOCABULARYTASK,
                    store: result.data
                })
            });
    };

export const editVocabularyTaskById = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_VOCABULARYTASK});
        await editVocabularyTask({token, body, id})
            .then(result => {
                dispatch({
                    type: C.EDIT_VOCABULARYTASK,
                    store: result.data
                })
            });
    };

export const removeVocabularyTaskById = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_VOCABULARYTASK});
        await removeVocabularyTask({token, id})
            .then(result => {
                dispatch({
                    type: C.REMOVE_VOCABULARYTASK,
                    store: result.data
                })
            });
    };
