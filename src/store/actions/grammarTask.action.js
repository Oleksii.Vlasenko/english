import C from '../consts';
import {
    getGrammarTasks,
    createGrammarTasks,
    editGrammarTask,
    removeGrammarTask
} from "../../services/grammarTasks.service";

export const saveGrammarTasks = () =>
    async (dispatch, token) => {
        dispatch({type: C.LOADING_GRAMMARTASK});
        await getGrammarTasks({token})
            .then(result => {
                dispatch({
                    type: C.SAVE_GRAMMARTASKS,
                    store: result.data
                })
            });
    };

export const addGrammarTask = () =>
    async (dispatch, token, body) => {
        dispatch({type: C.LOADING_GRAMMARTASK});
        await createGrammarTasks({token, body})
            .then(result => {
                dispatch({
                    type: C.CREATE_GRAMMARTASK,
                    store: result.data
                })
            })
    }
;

export const editGrammarTaskById = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_GRAMMARTASK});
        await editGrammarTask({token, body, id})
            .then(result => {
                dispatch({
                    type: C.EDIT_GRAMMARTASK,
                    store: result.data
                })
            })
    };

export const removeGrammarTaskById = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_GRAMMARTASK});
        await removeGrammarTask({token, id})
            .then(result => {
                dispatch({
                    type: C.REMOVE_GRAMMARTASK,
                    store: result.data
                })
            })
    };
