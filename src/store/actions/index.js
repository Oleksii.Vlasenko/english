import C from '../consts';

import {
    getGrammarThemes,
    getGrammarSubthemes
} from "../../services/grammarThemes.service";

import {editUser} from '../../services/users.service';

export const login = (authResponse) => ({
    type: C.LOGIN,
    store: authResponse
});

export const logout = () => ({
    type: C.LOGOUT
});

export const editMyOwnConfig = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_USER});
        await editUser({token, body, id})
            .then(result => {
                dispatch({
                    type: C.EDIT_USER,
                    store: {
                        firstName: result.data.firstName,
                        lastName: result.data.lastName
                    }
                })
            });
    };


export const saveThemes = () =>
    async (dispatch) => {
        await getGrammarThemes()
            .then(result => {
                dispatch({
                    type: C.SAVE_THEMES,
                    store: result.data
                })
            });

    };

export const saveSubthemes = () =>
    async (dispatch) => {
        await getGrammarSubthemes()
            .then(result => {
                dispatch({
                    type: C.SAVE_SUBTHEMES,
                    store: result.data
                })
            });
    };
