import C from '../consts';

import {
    getVocabularyThemes
} from "../../services/vocabularyThemes.service";

export const saveVocabularyThemes = () =>
    async (dispatch) => {
        dispatch({type: C.LOADING_VOCABULARYTHEMES});
        await getVocabularyThemes()
            .then(result => {
                dispatch({
                    type: C.SAVE_VOCABULARYTHEMES,
                    store: result.data
                })
            });
    };
