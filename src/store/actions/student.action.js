import C from '../consts';
import {
    getStudents,
    editStudent,
    removeStudent
} from "../../services/students.service";

export const saveStudents = () =>
    async (dispatch, token) => {
        dispatch({type: C.LOADING_STUDENT});
        await getStudents({token})
            .then(result => {
                dispatch({
                    type: C.GET_STUDENTS,
                    store: result.data
                })
            });
    };

export const editStudentById = () =>
    async (dispatch, token, body, id) => {
        dispatch({type: C.LOADING_STUDENT});
        await editStudent({token, body, id})
            .then(result => {
                dispatch({
                    type: C.EDIT_STUDENT,
                    store: result.data
                })
            });
    };

export const removeStudentById = () =>
    async (dispatch, token, id) => {
        dispatch({type: C.LOADING_STUDENT});
        await removeStudent({token, id})
            .then(result => {
                dispatch({
                    type: C.REMOVE_STUDENT,
                    store: result.data
                })
            })
    };
