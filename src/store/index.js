import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers";
// import {logger} from "redux-logger";
import thunk from "redux-thunk";

const configureStore = (initState = {theme: "dark"}) => {
    return createStore(
        rootReducer,
        initState,
        // applyMiddleware(logger, thunk));
        applyMiddleware(thunk));
};

export default configureStore;
