import C from '../../consts/index';

const initialState = {studentsList: [], isLoading: false};

const students = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_STUDENT:
            return {...state, isLoading: true};
        case C.GET_STUDENTS:
            return {
                studentsList: action.store,
                isLoading: false
            };
        case C.EDIT_STUDENT:
            return {
                ...state,
                studentsList: [
                    ...state.studentsList.map(item => item._id !== action.store._id ? item : action.store)
                ],
                isLoading: false
            };
        case C.REMOVE_STUDENT:
            return {
                ...state,
                studentsList: [
                    ...state.studentsList.filter(item => item._id !== action.store._id)
                ],
                isLoading: false
            };
        default:
            return state;
    }
};

export default students;
