import C from '../../consts/index';

const initialState = {marksList: [], isLoading: false};

const marks = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_MARK:
            return {...state, isLoading: true};
        case C.SAVE_MARKS:
            return {
                marksList: action.store,
                isLoading: false
            };
        case C.SAVE_MARKS_BY_TASKGROUP:
            return {
                ...state,
                marksList: [
                    ...state.marksList,
                    ...action.store
                ],
                isLoading: false
            };
        case C.CREATE_MARK:
            return {
                ...state,
                marksList: state.marksList.length > 0
                    ? [
                        ...state.marksList,
                        action.store
                    ]
                    : [
                        action.store
                    ],
                isLoading: false
            };
        case C.EDIT_MARK:
            return {
                ...state,
                marksList: [
                    ...state.marksList.filter(item => item._id !== action.store._id),
                    action.store
                ],
                isLoading: false
            };
        case C.REMOVE_MARK:
            return {
                ...state,
                marksList: [
                    ...state.marksList.filter(item => item._id !== action.store._id)
                ],
                isLoading: false
            };
        default:
            return state;
    }
};

export default marks;
