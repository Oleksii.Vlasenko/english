import C from '../../consts/index';

const initialState = {groupsList: [], isLoading: false};

const groups = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_GROUP:
            return {...state, isLoading: true};
        case C.SAVE_GROUPS:
            return {...state, groupsList: action.store, isLoading: false};
        case C.EDIT_GROUP:
            return {
                ...state,
                groupsList: [...state.groupsList.map(item => item._id !== action.store._id ? item : action.store)],
                isLoading: false
            };
        case C.CREATE_GROUP:
            return {
                ...state,
                groupsList: [...state.groupsList, action.store], isLoading: false};
        case C.REMOVE_GROUP:
            return {
                ...state,
                groupsList: [
                    ...state.groupsList.filter(item => item._id !== action.store._id)
                ],
                isLoading: false
            };
        default:
            return state;
    }
};

export default groups;
