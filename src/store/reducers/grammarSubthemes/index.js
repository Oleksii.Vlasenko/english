import C from '../../consts/index';

const initialState = [];

const grammarSubthemes = (state = initialState, action) => {
    switch (action.type) {
        case C.SAVE_SUBTHEMES:
            return action.store;
        default:
            return state;
    }
};

export default grammarSubthemes;
