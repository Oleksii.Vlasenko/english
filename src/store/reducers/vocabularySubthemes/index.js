import C from '../../consts/index';

const initialState = {vocabularySubthemesList: [], isLoading: false};

const vocabularySubthemes = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_VOCABULARYSUBTHEMES:
            return {
                ...state,
                isLoading: true
            };
        case C.SAVE_VOCABULARYSUBTHEMES:
            return {
                ...state,
                vocabularySubthemesList: [
                    ...state.vocabularySubthemesList,
                    ...action.store
                ],
                isLoading: false
            };
        default:
            return state;
    }
};

export default vocabularySubthemes;
