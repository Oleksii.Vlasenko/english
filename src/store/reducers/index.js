import {combineReducers} from "redux";
import auth from './auth/auth';
import grammarThemes from "./grammarThemes";
import grammarSubthemes from "./grammarSubthemes";
import groups from "./groups";
import taskGroups from "./taskGroups";
import students from "./students";
import grammarTasks from "./grammarTasks";
import vocabularyTasks from "./vocabularyTasks";
import marks from "./marks";
import vocabularyThemes from "./vocabularyThemes";
import vocabularySubthemes from "./vocabularySubthemes";
import loading from "./loading";

const rootReducer = combineReducers({
    auth,
    grammarThemes,
    grammarSubthemes,
    groups,
    taskGroups,
    students,
    grammarTasks,
    vocabularyTasks,
    marks,
    vocabularyThemes,
    vocabularySubthemes,
    loading
});

export default rootReducer;
