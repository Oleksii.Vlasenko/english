import C from '../../consts/index';

const initialState = {vocabularyThemesList: [], isLoading: false};

const vocabularyThemes = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_VOCABULARYTHEMES:
            return {
                ...state,
                isLoading: true
            };
        case C.SAVE_VOCABULARYTHEMES:
            return {
                ...state,
                vocabularyThemesList: [
                    ...state.vocabularyThemesList,
                    ...action.store
                ],
                isLoading: false
            };
        default:
            return state;
    }
};

export default vocabularyThemes;
