import C from '../../consts/index';

const initialState = [];

const grammarThemes = (state = initialState, action) => {
    switch (action.type) {
        case C.SAVE_THEMES:
            return action.store;
        default:
            return state;
    }
};

export default grammarThemes;
