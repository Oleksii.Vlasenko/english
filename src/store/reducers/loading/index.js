import C from "../../consts/index";

const initialState = false;

const loading = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_START:
            return true;
        case C.LOADING_END:
            return false;
        default:
            return state;
    }
};

export default loading;
