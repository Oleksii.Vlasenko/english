import C from '../../consts/index';

const initialState = {grammarTasksList: [], isLoading: false};

const grammarTasks = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_GRAMMARTASK:
            return {
                ...state,
                isLoading: true
            };
        case C.SAVE_GRAMMARTASKS:
            return {
                grammarTasksList: [
                    ...action.store
                ],
                isLoading: false
            };
        case C.CREATE_GRAMMARTASK:
            return {
                ...state,
                grammarTasksList: [
                    ...state.grammarTasksList,
                    action.store
                ],
                isLoading: false
            };
        case C.EDIT_GRAMMARTASK:
            return {
                ...state,
                grammarTasksList: [
                    ...state.grammarTasksList.map(item =>
                        item._id !== action.store._id ? item : action.store)
                ],
                isLoading: false
            };
        case C.REMOVE_GRAMMARTASK:
            return {
                ...state,
                grammarTasksList: [
                    ...state.grammarTasksList.filter(item => item._id !== action.store._id)
                ],
                isLoading: false
            };
        default:
            return state;
    }
};

export default grammarTasks;
