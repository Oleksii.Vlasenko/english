import C from '../../consts/index';

const initialState = {taskGroupsList: [], isLoading: false};

const taskGroups = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_TASKGROUP:
            return {...state, isLoading: true};
        case C.SAVE_TASKGROUPS:
            return {...state, taskGroupsList: action.store, isLoading: false};
        case C.SAVE_STUDENTTASKGROUPS:
            return {...state, taskGroupsList: action.store, isLoading: false};
        case C.CREATE_TASKGROUP:
            return {
                ...state,
                taskGroupsList: [...state.taskGroupsList, action.store], isLoading: false
            };
        case C.EDIT_TASKGROUP:
            return {
                ...state,
                taskGroupsList: [...state.taskGroupsList.map(item => item._id !== action.store._id ? item : action.store)],
                isLoading: false
            };
        case C.REMOVE_TASKGROUP:
            return {
                ...state,
                taskGroupsList: [...state.taskGroupsList.filter(item => item._id !== action.store._id)],
                isLoading: false
            };
        default:
            return state;
    }
};

export default taskGroups;
