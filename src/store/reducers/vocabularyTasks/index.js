import C from '../../consts/index';

const initialState = {vocabularyTasksList: [], isLoading: false};

const vocabularyTasks = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_VOCABULARYTASK:
            return {...state, isLoading: true};
        case C.SAVE_VOCABULARYTASKS:
            return {
                vocabularyTasksList: action.store,
                isLoading: false
            };
        case C.CREATE_VOCABULARYTASK:
            return {
                ...state,
                vocabularyTasksList: [
                    ...state.vocabularyTasksList,
                    action.store
                ],
                isLoading: false
            };
        case C.EDIT_VOCABULARYTASK:
            return {
                ...state,
                vocabularyTasksList:
                    state.vocabularyTasksList.find(item => item._id === action.store._id)
                        ? [...state.vocabularyTasksList.map(item => item._id !== action.store._id ? item : action.store)]
                        : [...state.vocabularyTasksList, action.store],
                isLoading: false
            };
        case C.REMOVE_VOCABULARYTASK:
            return {
                ...state,
                vocabularyTasksList: [
                    ...state.vocabularyTasksList.filter(item => item._id !== action.store._id)
                ],
                isLoading: false
            };
        default:
            return state;

    }
};

export default vocabularyTasks;
