import C from '../../consts/index';

const initialState = {user: {}, isLoading: false};

const auth = (state = initialState, action) => {
    switch (action.type) {
        case C.LOADING_USER:
            return {
                ...state,
                isLoading: true
            };
        case C.LOGIN:
            return {
                ...state,
                user: {
                    ...action.store
                },
                isLoading: false
            };
        case C.LOGOUT:
            return {
                ...state,
                user: {},
                isLoading: false
            };
        case C.EDIT_USER:
            return {
                ...state,
                user: {
                    ...state.user,
                    firstName: action.store.firstName,
                    lastName: action.store.lastName
                },
                isLoading: false
            };
        default:
            return state;
    }
};

export default auth;
