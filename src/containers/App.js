import React from 'react';
import Header from "../components/Header";
import Section from "../components/Section";
import {BrowserRouter} from "react-router-dom";
import configureStore from "../store";
import useAuth from "../hooks/auth.hook";
import {Provider} from "react-redux";


function App() {

    const {login, logout, token, firstName, lastName, role, id} = useAuth();
    const store = configureStore({
        auth: {
            user: {
                login, logout, token, firstName, lastName, role, id
            },
            isLoading: false
        }
    });

    return (
        <Provider store={store}>
            <BrowserRouter>
                <div>
                    <Header/>
                    <Section/>
                </div>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
