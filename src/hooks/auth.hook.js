import {useState, useCallback, useEffect} from "react";

const storage = "userData";

const useAuth = () => {
    const [token, setToken] = useState(null);
    const [firstName, setFirstName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [role, setRole] = useState(null);
    const [id, setId] = useState(null);

    const login = useCallback((jwtToken, firstName, lastName, role, id) => {
        setToken(jwtToken);
        setRole(role);
        setFirstName(firstName);
        setLastName(lastName);
        setId(id);
        localStorage.setItem(storage, JSON.stringify({role, token: jwtToken, lastName, firstName, id}));

    }, []);

    const logout = useCallback(() => {
        setToken(null);
        setRole(null);
        setFirstName(null);
        setLastName(null);
        setId(null);
        localStorage.removeItem(storage);
    }, []);

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storage));

        if (data && data.token) {
            login(data.token, data.firstName, data.lastName, data.role, data.id);
        }
    }, [login]);

    return {login, logout, token, role, firstName, lastName, id};
};

export default useAuth;
