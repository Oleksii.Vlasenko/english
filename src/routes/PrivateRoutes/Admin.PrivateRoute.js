import React, {useEffect} from "react";
import { Route, Redirect } from 'react-router-dom';

const AdminPrivateRoute = ({component: Component, ...rest}) => {
    let role = "admin";
    useEffect(() => {
        if (localStorage.getItem('userData')) {
            role = JSON.parse(localStorage.getItem('userData')).role;
        }
    });
    return (
        <Route
            {...rest}
            render={props => (role === "admin")
                ? <Component {...props} />
                : <Redirect to="/" />
            }
        />
    )
};

export default AdminPrivateRoute;
