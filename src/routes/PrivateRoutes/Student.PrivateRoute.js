import React, {useEffect} from "react";
import { Route, Redirect } from 'react-router-dom';

const StudentPrivateRoute = ({component: Component, ...rest}) => {
    let role = "student";
    useEffect(() => {
        if (localStorage.getItem('userData')) {
            role = JSON.parse(localStorage.getItem('userData')).role;
        }
    });
    return (
        <Route
            {...rest}
            render={props => (role === "admin" || role === "teacher" || role === "student")
                ? <Component {...props} />
                : <Redirect to="/" />
            }
        />
    )
};

export default StudentPrivateRoute;
