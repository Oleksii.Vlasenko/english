import React, {useEffect} from "react";
import { Route, Redirect } from 'react-router-dom';

const TeacherPrivateRoute = ({component: Component, ...rest}) => {
    let role = "teacher";
    useEffect(() => {
        if (localStorage.getItem('userData')) {
            role = JSON.parse(localStorage.getItem('userData')).role;
        }
    });
    return (
        <Route
            {...rest}
            render={props => (role === "admin" || role === "teacher")
                ? <Component {...props} />
                : <Redirect to="/" />
            }
        />
    )
};

export default TeacherPrivateRoute;
