import React from "react";
import {Route, Switch} from "react-router";
import HomePage from "../components/Section/HomePage";
import StaffroomUsers from "../components/Section/Staffroom/StaffroomStudents";
import Tasks from "../components/Section/Tasks";
import Administration from "../components/Section/Staffroom/Administration";
import StaffroomTasks from "../components/Section/Staffroom/StaffroomTasks";
import StaffroomAddTask from "../components/Section/Staffroom/StaffroomTasks/StaffroomTaskGroups/StaffroomAddTask";
import GramPracSimple from "../components/Section/Practice/GrammarPractice/GramPracSimple";
import StandardGrammarTasks from "../components/Section/StandardTasks/StandardGrammarTasks";
import Task from "../components/Section/Tasks/TasksItem/Task";
import StaffroomMarks from "../components/Section/Staffroom/StaffroomMarks";
import GrammarPractice from "../components/Section/Practice/GrammarPractice";
import GramPracNormal from "../components/Section/Practice/GrammarPractice/GramPracNormal";

import AdminPrivateRoute from "./PrivateRoutes/Admin.PrivateRoute";
import TeacherPrivateRoute from "./PrivateRoutes/Teacher.PrivateRoute";
import StudentPrivateRoute from "./PrivateRoutes/Student.PrivateRoute";
import StandardVocabularyTasks from "../components/Section/StandardTasks/StandardVocabularyTasks";
import VocabularyPractice from "../components/Section/Practice/VocabularyPractice";
import VocPracTheory from "../components/Section/Practice/VocabularyPractice/VocPracTheory";
import VocPracNormal from "../components/Section/Practice/VocabularyPractice/VocPracNormal";
import VocPracSimple from "../components/Section/Practice/VocabularyPractice/VocPracSimple";
import VocPracHard from "../components/Section/Practice/VocabularyPractice/VocPracHard";
import ErrorBoundary from "../components/Section/ErrorBoundary";
import GramPracHard from "../components/Section/Practice/GrammarPractice/GramPracHard";
import About from "../components/Section/About";

const SectionRoutes = () => {
    return (
        <Switch>
<ErrorBoundary>
            <Route path="/" component={() => <HomePage/>} exact/>
            <Route path="/about" component={() => <About/>} exact/>

            <Route path="/practice/grammar" component={() => <GrammarPractice/>} exact/>
            <Route path="/practice/grammar/simple" component={() => <GramPracSimple/>}/>
            <Route path="/practice/grammar/normal" component={() => <GramPracNormal/>}/>
            <Route path="/practice/grammar/hard" component={() => <GramPracHard/>}/>

            <Route path="/practice/vocabulary" component={() => <VocabularyPractice/>} exact/>
            <Route path="/practice/vocabulary/theory" component={() => <VocPracTheory/>}/>
            <Route path="/practice/vocabulary/simple" component={() => <VocPracSimple/>}/>
            <Route path="/practice/vocabulary/normal" component={() => <VocPracNormal/>}/>
            <Route path="/practice/vocabulary/hard" component={() => <VocPracHard/>}/>

            <StudentPrivateRoute path="/tasks" component={() => <Tasks/>} exact/>
            <StudentPrivateRoute path="/task/" component={() => <Task/>} exact/>

            <TeacherPrivateRoute path="/class/groups" component={() => <StaffroomUsers/>} exact/>
            <TeacherPrivateRoute path="/class/tasks" component={() => <StaffroomTasks/>} exact/>
            <TeacherPrivateRoute path="/class/tasks/newtask" component={() => <StaffroomAddTask/>} exact/>
            <TeacherPrivateRoute path="/class/marks" component={() => <StaffroomMarks/>} exact/>

            <TeacherPrivateRoute path="/standard/grammar" component={() => <StandardGrammarTasks/>} exact/>
                <TeacherPrivateRoute path="/standard/vocabulary" component={() => <StandardVocabularyTasks/>} exact/>

            <AdminPrivateRoute path="/staffroom/administration" component={() => <Administration/>} exact/>
</ErrorBoundary>
        </Switch>

    );


};

export default SectionRoutes;
