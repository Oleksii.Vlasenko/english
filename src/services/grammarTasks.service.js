import axios from 'axios';

export const getGrammarTasks = ({token}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/grammar/task`, {
        headers: {
            authorization: token
        }
    });
};

export const getGrammarTaskById = id =>
    axios.get(`https://english-app-v.herokuapp.com/api/grammar/task/${id}`);

export const createGrammarTasks = ({token, body}) => {
    return axios.post(`https://english-app-v.herokuapp.com/api/grammar/task`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const editGrammarTask = ({token, body, id}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/grammar/task/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const removeGrammarTask = ({token, id}) => {
    return axios.delete(`https://english-app-v.herokuapp.com/api/grammar/task/${id}`, {
        headers: {
            authorization: token
        }
    });
};
