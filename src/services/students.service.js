import axios from 'axios';

export const getStudents = ({token}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/user`, {
        headers: {
            authorization: token
        }
    });
};

export const editStudent = ({token, body, id}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/user/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const removeStudent = ({token, id}) => {
    return axios.delete(`https://english-app-v.herokuapp.com/api/user/${id}`, {
        headers: {
            authorization: token
        }
    });
};
