import axios from 'axios';

export const getVocabularySubthemes = () => {
    return axios.get(`https://english-app-v.herokuapp.com/api/vocabulary/subtheme`);
};
