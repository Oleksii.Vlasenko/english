import axios from 'axios';

export const getMarks = ({token}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/mark`, {
        headers: {
            authorization: token
        }
    });
};

export const getMarksByTaskGroup = ({token, id}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/mark/${id}`, {
        headers: {
            authorization: token
        }
    });
};

export const createMark = ({token, body}) => {
    return axios.post(`https://english-app-v.herokuapp.com/api/mark`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const editMark = ({token, body, id}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/mark/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const removeMark = ({token, id}) => {
    return axios.delete(`https://english-app-v.herokuapp.com/api/mark/${id}`, {
        headers: {
            authorization: token
        }
    });
};
