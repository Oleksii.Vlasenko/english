import axios from 'axios';

export const getGrammarThemes = () => {
    return axios.get(`https://english-app-v.herokuapp.com/api/grammar/theme`);
};

export const getGrammarSubthemes = () => {
    return axios.get(`https://english-app-v.herokuapp.com/api/grammar/subtheme`);
};
