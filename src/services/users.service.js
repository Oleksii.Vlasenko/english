import axios from 'axios';

export const editUser = ({token, body, id}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/user/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const resetUserPassword = ({token, id}) => {
    axios.put(`https://english-app-v.herokuapp.com/api/user/${id}`,
        JSON.stringify({password: "english"}),
        {
            headers: {
                authorization: token,
                "Content-Type": "application/json"
            }
        });
};


export const getUsers = ({token, headers}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/user`, {
        headers: {
            ...headers,
            authorization: token
        }
    });
};
