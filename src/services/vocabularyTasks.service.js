import axios from 'axios';

export const getVocabularyTasks = ({token, id}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/vocabulary/task/${id}`, {
        headers: {
            authorization: token
        }
    });
};

export const getVocabularyTaskById = id =>
    axios.get(`https://english-app-v.herokuapp.com/api/vocabulary/task/${id}`);

export const getVocabulary = (token, id, value) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/vocabulary/task`, {
        headers: {
            authorization: token,
            getlist: 20,
            taskGroupId: id,
            searchstring: value
        }
    });
};

export const createVocabularyTasks = ({token, body}) => {
    return axios.post('https://english-app-v.herokuapp.com/api/vocabulary/Task',
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const editVocabularyTask = ({token, body, id}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/vocabulary/task/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const removeVocabularyTask = ({token, id}) => {
    return axios.delete(`https://english-app-v.herokuapp.com/api/vocabulary/task/${id}`, {
        headers: {
            authorization: token
        }
    });
};
