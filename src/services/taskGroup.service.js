import axios from 'axios';

export const getTaskGroups = ({token}) => {
    return axios.get('https://english-app-v.herokuapp.com/api/taskgroup', {
        headers: {
            authorization: token
        }
    });
};

export const getStudentTaskGroups = ({token, id}) => {
    return axios.get(`https://english-app-v.herokuapp.com/api/taskgroup/${id}`, {
        headers: {
            authorization: token
        }
    });
};

export const createTaskGroups = ({token, body}) => {
    return axios.post('https://english-app-v.herokuapp.com/api/taskgroup',
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const removeTaskGroup = ({token, id}) => {
    return axios.delete(`https://english-app-v.herokuapp.com/api/taskgroup/${id}`, {
        headers: {
            authorization: token
        }
    });
};

export const editTaskGroup = ({token, id, body}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/taskgroup/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};
