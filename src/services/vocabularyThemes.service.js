import axios from 'axios';

export const getVocabularyThemes = () => {
    return axios.get(`https://english-app-v.herokuapp.com/api/vocabulary/theme`);
};
