import axios from 'axios';

export const getGroups = (token) => {
    return axios.get('https://english-app-v.herokuapp.com/api/group', {
        headers: {
            authorization: token
        }
    });
};

export const editGroup = ({token, body, id}) => {
    return axios.put(`https://english-app-v.herokuapp.com/api/group/${id}`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const createGroup = ({token, body}) => {
    return axios.post(`https://english-app-v.herokuapp.com/api/group/`,
        JSON.stringify(body),
        {
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            }
        });
};

export const removeGroup = ({token, id}) => {
    return axios.delete(`https://english-app-v.herokuapp.com/api/group/${id}`, {
        headers: {
            authorization: token
        }
    });
};
