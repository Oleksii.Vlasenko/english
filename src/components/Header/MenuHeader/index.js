import React, {useEffect, useState} from 'react';
import {Navbar, Nav, Container} from "react-bootstrap";
import MenuHeaderItem from "./MenuHeaderItem";
import MyVerticallyLoginCenteredModal from "../LoginModal";
import MyVerticallyRegisterCenteredModal from '../RegisterModal';
import {useSelector} from "react-redux";
import {Link, useHistory} from "react-router-dom";
import {faSignOutAlt, faSignInAlt, faUserPlus, faUserCog} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ConfigureModal from "../ConfigModal";

export default function MenuHeader() {
    const state = useSelector(state => state);
    const {
        auth,
        grammarTasks,
        groups,
        marks,
        students,
        taskGroups,
        vocabularySubthemes,
        vocabularyTasks,
        vocabularyThemes,
        loading
    } = state;
    const {user} = auth;

    const [load, setLoad] = useState(false);

    useEffect(() => {
        const load = auth.isLoading
            || grammarTasks.isLoading
            || groups.isLoading
            || marks.isLoading
            || students.isLoading
            || taskGroups.isLoading
            || vocabularySubthemes.isLoading
            || vocabularyTasks.isLoading
            || vocabularyThemes.isLoading
            || loading;
        setLoad(load);
    }, [auth, grammarTasks, groups, marks, students, taskGroups, vocabularySubthemes, vocabularyTasks, vocabularyThemes]);

    const [loginShow, setLoginShow] = useState(false);
    const [registerShow, setRegisterShow] = useState(false);
    const [configShow, setConfigShow] = useState(false);
    const [expanded, setExpanded] = useState(false);
    const standard = ["Grammar", "Vocabulary"];
    const practices = ["Grammar", "Vocabulary"];
    const classes = ["Groups", "Tasks", "Marks"];
    const history = useHistory();

    const expendHandler = () => {
        setExpanded(false);
    };

    const logoutHandler = () => {
        user.logout();
        history.push("/");
    };

    return (
        <React.Fragment>
            <Navbar collapseOnSelect
                    expand="lg"
                    expanded={expanded}
                    sticky="top"
                    className="py-lg-2 py-md-2"
                    style={{backgroundColor: "#E9F6FE"}}
                    id="main-header"
            >
                <Container>
                    <Navbar.Brand onClick={expendHandler}
                                  className="mr-1">
                        <Link to="/" className="text-decoration-none">
                            <span className="text-dark-blue font-weight-bold">JUST ENGLISH</span>
                        </Link>
                    </Navbar.Brand>
                    <div
                        className={`spinner-border spinner-border-sm mr-5 text-danger ${load ? "visible" : "invisible"}`}
                        role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"
                                   onClick={() => setExpanded(expanded ? false : "expanded")}/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <MenuHeaderItem title={"Practice"}
                                            list={practices}
                                            action={expendHandler}/>
                            {user.role === "student"
                            && <Nav.Link className="mr-5">
                                <Link onClick={expendHandler}
                                      to="/tasks"
                                      className="text-decoration-none"
                                      style={{color: "inherit"}}>
                                    Tasks
                                </Link>
                            </Nav.Link>}
                            {user.role === "teacher" && <MenuHeaderItem title={"Class"}
                                                                        list={classes}
                                                                        action={expendHandler}/>}
                            {user.role === "admin" && <MenuHeaderItem title={"Standard"}
                                                                      list={standard}
                                                                      action={expendHandler}/>}
                            {user.role === "admin"
                            && <Nav.Link className="mr-5">
                                <Link onClick={expendHandler}
                                      to="/staffroom/administration"
                                      className="text-decoration-none"
                                      style={{color: "inherit"}}>
                                    Administration
                                </Link>
                            </Nav.Link>}
                            <Nav.Link className="mr-5">
                                <Link onClick={expendHandler}
                                      to="/about"
                                      className="text-decoration-none text-dark-blue"
                                      style={{color: "inherit"}}
                                >
                                    About
                                </Link>
                            </Nav.Link>

                        </Nav>
                        <Nav>
                            <div className="p-0 py-3">
                                {!(user.role) &&
                                <span className="text-dark-blue mr-3 pointer ml-3"
                                      onClick={() => setLoginShow(true)}>
                                    Sign in<FontAwesomeIcon icon={faSignInAlt}
                                                            className="mx-2"/>
                                </span>}
                                {!(user.role) &&
                                <span className="text-dark-blue float-right pointer ml-3"
                                      onClick={() => setRegisterShow(true)}>
                                    Sign up<FontAwesomeIcon icon={faUserPlus}
                                                            className="ml-2"/>
                                </span>}
                                {(!!user.role) &&
                                <span className="mr-1 text-dark-blue pointer ml-3"
                                      onClick={() => setConfigShow(true)}>
                                    <span className="text-dark-blue mr-2">
                                        {user.lastName}
                                    </span>
                                    <FontAwesomeIcon icon={faUserCog}
                                                     className="text-dark-blue"/>
                                </span>}
                                {(!!user.role) &&
                                <span className="text-dark-blue float-right pointer ml-3"
                                      onClick={logoutHandler}>
                                    Sign out<FontAwesomeIcon icon={faSignOutAlt}
                                                             className="text-dark-blue ml-2"/>
                                </span>}
                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <ConfigureModal
                show={configShow}
                onHide={() => setConfigShow(false)}
            />

            <MyVerticallyLoginCenteredModal
                show={loginShow}
                onHide={() => setLoginShow(false)}
            />
            <MyVerticallyRegisterCenteredModal
                show={registerShow}
                onHide={() => setRegisterShow(false)}
            />
        </React.Fragment>
    );
}
