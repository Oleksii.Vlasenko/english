import React from 'react';
import {NavDropdown} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function MenuHeaderItem({title, list, action}) {

    const MenuItems = () => {
        const itemsList = list.map((item, index) => {
            let link = `/${title.toLowerCase()}/${item.toLowerCase()}`;
            return (
                <NavDropdown.Item key={index}>
                    <Link to={link}
                          className="ml-3 text-decoration-none text-dark-blue"
                          onClick={action}
                    >
                        {item}
                    </Link>
                </NavDropdown.Item>
            )
        });
        return itemsList;
    };

    return (
        <NavDropdown title={title} className="mr-5">
            {MenuItems()}
        </NavDropdown>
    );


}
