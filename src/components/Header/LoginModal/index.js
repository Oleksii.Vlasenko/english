import {Alert, Button, Form, Modal} from "react-bootstrap";
import React, {useState} from "react";
import useHttp from "../../../hooks/http.hook";
import {useDispatch, useSelector} from "react-redux";
import {login} from '../../../store/actions/index';
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import AlertEvent from "../../AlertEvent";
import C from "../../../store/consts/index"

const MyVerticallyLoginCenteredModal = (props) => {
    const {request} = useHttp();

    const {user} = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [form, setForm] = useState({
        login: "",
        password: ""
    });
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    const loginHandler = async () => {
        try {
            dispatch({type: C.LOADING_START});
            const data = await request('https://english-app-v.herokuapp.com/api/login', 'POST', {...form});
            dispatch({type: C.LOADING_END});
            if (data.token) {
                props.onHide();
                const authData = {
                    token: data.token,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    role: data.role,
                    id: data.id
                };
                dispatch(login(authData));
                user.login(data.token, data.firstName, data.lastName, data.role, data.id);
            }
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value});
    };

    return (
        <React.Fragment>
            {show &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShow(false)}
                        message={alertForm.message} />}
            <Modal
                {...props}
                size="md"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title id="contained-modal-title-vcenter">
                        LOGIN
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Login</Form.Label>
                            <Form.Control name="login"
                                          type="text"
                                          placeholder="Enter login"
                                          onChange={changeHandler}/>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control name="password"
                                          type="password"
                                          placeholder="Enter password"
                                          onChange={changeHandler}/>
                        </Form.Group>

                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    <Button disabled={!(form.login && form.password)}
                            onClick={loginHandler}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue"
                    >
                        <FontAwesomeIcon icon={faCheck} className="mx-2 text-light"/>
                    </Button>
                    <Button onClick={() => props.onHide()}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue"
                    >
                        <FontAwesomeIcon icon={faTimes} className="mx-2 text-light"/>
                    </Button>
                </Modal.Footer>
            </Modal>

        </React.Fragment>
    );
};

export default MyVerticallyLoginCenteredModal;
