import {Button, Col, Form, Modal} from "react-bootstrap";
import React, {useState} from "react";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useDispatch, useSelector} from "react-redux";
import {editMyOwnConfig} from "../../../store/actions/index";
import AlertEvent from "../../AlertEvent";
import C from "../../../store/consts";

const ConfigureModal = (props) => {
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.auth);
    const {token, lastName, role, id, login} = user;

    const [form, setForm] = useState({
        login: undefined,
        password: undefined,
        firstName: undefined,
        lastName: undefined,
        email: undefined
    });
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    const registerHandler = async () => {
        try {
            dispatch({type: C.LOADING_START});
            await editMyOwnConfig()(dispatch, token, {...form}, id);
            dispatch({type: C.LOADING_END});
            // login(token, firstName, lastName, role, id);
            props.onHide();
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value});
    };

    return (
        <React.Fragment>
            {show &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShow(false)}
                        message={alertForm.message} />}
            <Modal
                {...props}
                size="md"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >

                <Modal.Header closeButton
                              className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title id="contained-modal-title-vcenter">
                        CONFIGURE
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col}
                                        controlId="formGridEmail">
                                <Form.Label>Login</Form.Label>
                                <Form.Control type="text"
                                              placeholder="Enter login"
                                              name="login"
                                              onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password"
                                              placeholder="Password"
                                              name="password"
                                              onChange={changeHandler}/>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridFirstName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control placeholder="Enter first name"
                                              name="firstName"
                                              onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridLastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control placeholder="Enter last name"
                                              name="lastName"
                                              onChange={changeHandler}/>
                            </Form.Group>
                        </Form.Row>
                        <Form.Group controlId="formGridEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email"
                                          type="email"
                                          placeholder="Enter email"
                                          onChange={changeHandler}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    <Button onClick={registerHandler}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue">
                        <FontAwesomeIcon icon={faCheck} className="mx-2 text-light"/>
                    </Button>
                    <Button onClick={() => props.onHide()}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue">
                        <FontAwesomeIcon icon={faTimes} className="mx-2 text-light"/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    );
};

export default ConfigureModal;
