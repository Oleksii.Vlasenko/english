import {Button, Col, Form, Modal} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import useHttp from "../../../hooks/http.hook";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {login} from "../../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import AlertEvent from "../../AlertEvent";
import C from "../../../store/consts/index";

const MyVerticallyRegisterCenteredModal = (props) => {
    const {request} = useHttp();
    const {user} = useSelector(state => state.auth);

    const dispatch = useDispatch();

    const [form, setForm] = useState({
        login: undefined,
        password: undefined,
        firstName: undefined,
        lastName: undefined,
        role: "student",
        email: undefined
    });

    const [border, setBorder] = useState({
        login: true,
        password: true,
        firstName: true,
        lastName: true,
        email: true,
        groupKey: true
    });

    useEffect(() => {
        setBorder({
            login: true,
            password: true,
            firstName: true,
            lastName: true,
            email: true,
            groupKey: true
        });
    }, [props.show]);

    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    const formValidate = () => {
        let valid = true;
        const checkBorder = {...border};
        if (!form.login || form.login.length < 6 || form.login.match("^[a-zA-Z0-9-_]*$") === null) {
            checkBorder.login = false;
            valid = false;
        }
        if (!form.password || form.password.length < 8) {
            checkBorder.password = false;
            valid = false;
        }
        if (!form.firstName || form.firstName.match(/^\p{Lu}/gu) === null || form.firstName.match("^[a-zA-Z-]*$") === null) {
            checkBorder.firstName = false;
            valid = false;
        }
        if (!form.lastName || form.lastName.match(/^\p{Lu}/gu) === null || form.lastName.match("^[a-zA-Z-]*$") === null) {
            checkBorder.lastName = false;
            valid = false;
        }

        if (!form.email || form.email.length < 5 || form.email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) === null) {
            checkBorder.email = false;
            valid = false;
        }
        if (!form.groupKey && form.role === "student") {
            checkBorder.groupKey = false;
            valid = false;
        }
        setBorder({...border, ...checkBorder});
        return valid;
    };

    const registerHandler = async () => {
        try {
            if (formValidate()) {
                dispatch({type: C.LOADING_START});
                const data = await request('https://english-app-v.herokuapp.com/api/register', 'POST', {...form});
                dispatch({type: C.LOADING_END});
                if (data.token) {
                    props.onHide();
                    if (data.role === "student") {
                        const authData = {
                            token: data.token,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            role: data.role,
                            id: data.id
                        };
                        dispatch(login(authData));
                        user.login(data.token, data.firstName, data.lastName, data.role, data.id);
                        setAlertForm({message: "You have successfully registered", variant: "success"});
                        setShow(true);
                    } else {
                        setAlertForm({
                            message: "You have successfully registered. Wait for confirmation.",
                            variant: "success"
                        });
                        setShow(true);
                    }
                }
            }
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    const roleHandler = (event) => {
        setForm({...form, role: event.target.id});
    };

    const changeHandler = event => {
        setBorder({...border, [event.target.name]: true});
        setForm({...form, [event.target.name]: event.target.value});
    };

    return (
        <React.Fragment>
            {show &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShow(false)}
                        message={alertForm.message}/>}
            <Modal
                {...props}
                size="md"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >

                <Modal.Header closeButton className="text-light bg-light-blue rounded-2rem-up">
                    <Modal.Title id="contained-modal-title-vcenter">
                        REGISTRATION
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Label>Login</Form.Label>
                                <Form.Control type="text"
                                              placeholder="Enter login"
                                              name="login"
                                              onChange={changeHandler}
                                              className={`${!border.login && "border-danger"}`}
                                />
                                <Form.Text className="text-muted">
                                    ! minimum 6 signs
                                </Form.Text>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password"
                                              placeholder="Password"
                                              name="password"
                                              onChange={changeHandler}
                                              className={`${!border.password && "border-danger"}`}
                                />
                                <Form.Text className="text-muted">
                                    ! minimum 8 signs
                                </Form.Text>
                            </Form.Group>
                        </Form.Row>

                        <Form.Group controlId="formGridFirstName">
                            <Form.Label>First name</Form.Label>
                            <Form.Control placeholder="Enter first name"
                                          name="firstName"
                                          onChange={changeHandler}
                                          className={`${!border.firstName && "border-danger"}`}
                            />
                        </Form.Group>

                        <Form.Group controlId="formGridLastName">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control placeholder="Enter last name"
                                          name="lastName"
                                          onChange={changeHandler}
                                          className={`${!border.lastName && "border-danger"}`}
                            />
                        </Form.Group>

                        <Form.Group controlId="formGridEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email"
                                          type="email"
                                          placeholder="Enter email"
                                          onChange={changeHandler}
                                          className={`${!border.email && "border-danger"}`}
                            />
                        </Form.Group>

                        <Form.Group>
                            <Form.Check
                                name="role"
                                custom
                                inline
                                label="Teacher"
                                type="radio"
                                id="teacher"
                                checked={form.role === "teacher"}
                                onChange={roleHandler}
                            />
                            <Form.Check
                                name="role"
                                custom
                                inline
                                label="Student"
                                type="radio"
                                id="student"
                                checked={form.role === "student"}
                                onChange={roleHandler}
                            />
                        </Form.Group>
                        {(form.role === "student") && <Form.Group controlId="formGridGroupKey"
                                                                  sm="xl">
                            <Form.Label>Your key</Form.Label>
                            <Form.Control placeholder="Enter your key"
                                          name="groupKey"
                                          onChange={changeHandler}
                                          className={`${!border.groupKey && "border-danger"}`}
                            />
                            <Form.Text className="text-muted">
                                ! You can take from your teacher
                            </Form.Text>
                        </Form.Group>}
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    <Button onClick={registerHandler}
                            className="px-sm-5 px-xs-4 bg-light-blue task-btn">
                        <FontAwesomeIcon icon={faCheck}
                                         className="mx-2 text-light"/>
                    </Button>
                    <Button onClick={() => props.onHide()}
                            className="px-sm-5 px-xs-4 bg-light-blue task-btn">
                        <FontAwesomeIcon icon={faTimes}
                                         className="mx-2 text-light"/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    );
};

export default MyVerticallyRegisterCenteredModal;
