import React, {useEffect} from "react";
import {Alert} from "react-bootstrap";

const AlertEvent = ({message, hide, variant}) => {

    const hideAlert = () => {
        hide();
    };

    useEffect(() => {
        setTimeout(hideAlert, 4000);
    }, [message]);

    return (
        <React.Fragment>
            <Alert variant={variant || "info"}
                   onClose={() => hide()}
                   dismissible
                   className="mr-md-5 m-sm-0 alert-message">
                {message}
            </Alert>
        </React.Fragment>
    )
};

export default AlertEvent;
