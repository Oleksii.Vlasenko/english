import React from 'react';
import {Carousel, Col, Container, Row} from "react-bootstrap";

const About = () => {

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component about">
                <Carousel className="h-100 w-100 about-carousel">
                    <Carousel.Item className="h-100 w-100 about-carousel">
                        <h1 className="text-center pb-5 d-none d-md-block text-dark-blue font-dancing-script">For teachers</h1>
                        <Row className="h-100" md="3" sm="2" xs="1">
                            <Col className="h-100 text-center">
                                <img src={require("../../../images/add_group.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 h-50"/>
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Create your own group
                                    </h3>
                                    <p>
                                        Add your students to groups and test their knowledge!
                                    </p>
                                </div>
                            </Col>
                            <Col className="h-100 d-none d-sm-block text-center">
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Create tasks for your groups
                                    </h3>
                                    <p>
                                        Create homework, tests, etc. and monitor students` achievement.
                                    </p>
                                </div>
                                <img src={require("../../../images/add_taskgroup.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 h-25"/>
                            </Col>
                            <Col className="h-100 d-none d-lg-block">
                                <img src={require("../../../images/add_voctask.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-50"/>
                                <img src={require("../../../images/add_grtask.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-50"/>
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Creating homework becomes easier
                                    </h3>
                                    <p>
                                        Use the tasks of other teachers or create your own.
                                    </p>
                                </div>
                            </Col>
                        </Row>
                    </Carousel.Item>
                    <Carousel.Item className="h-100 w-100">
                        <h1 className="text-center pb-5 d-none d-md-block text-dark-blue font-dancing-script">Grammar tasks</h1>
                        <Row className="h-100" md="3" sm="2" xs="1">
                            <Col className="h-100">
                                <img src={require("../../../images/pr_gr_easy.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-100"/>
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Multiple choice
                                    </h3>
                                    <p>
                                        You are offered several answer options.
                                        Choose the grammatically correct answer and get the highest score!
                                    </p>
                                </div>
                            </Col>
                            <Col className="h-100 d-none d-sm-block">
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Scrambled sentences
                                    </h3>
                                    <p>
                                        Put the words into correct order and make the sentence!
                                    </p>
                                </div>
                                <img src={require("../../../images/pr_gr_normal.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-100"/>
                            </Col>
                            <Col className="h-100 d-none d-lg-block">
                                <img src={require("../../../images/pr_gr_hard.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-100"/>
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Fill in the blank
                                    </h3>
                                    <p>
                                        Complete the sentences with the correct word!
                                    </p>
                                </div>
                            </Col>
                        </Row>
                    </Carousel.Item>
                    <Carousel.Item className="h-100 w-100">
                        <h1 className="text-center pb-5 d-none d-md-block text-dark-blue font-dancing-script">
                            Vocabulary tasks
                        </h1>
                        <Row className="h-100" md="3" sm="2" xs="1">
                            <Col className="h-100">
                                <img src={require("../../../images/pr_voc_easy.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-100"/>
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Flash cards
                                    </h3>
                                    <p>
                                        Memorize words with cards.
                                        All you have to do is turn the translation!
                                    </p>
                                </div>
                            </Col>
                            <Col className="h-100 d-none d-sm-block">
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Multiple choice
                                    </h3>
                                    <p>
                                        You are offered several answer options.
                                        Choose the grammatically correct answer and get the highest score!
                                    </p>
                                </div>
                                <img src={require("../../../images/pr_voc_normal.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-100"/>
                            </Col>
                            <Col className="h-100 d-none d-lg-block">
                                <img src={require("../../../images/pr_voc_hard.PNG")}
                                     alt="Grammar easy"
                                     className="p-3 w-100"/>
                                <div className="h-50 text-center py-5">
                                    <h3>
                                        Open-ended
                                    </h3>
                                    <p>
                                        Enter the correct translation of the word.
                                    </p>
                                </div>
                            </Col>
                        </Row>
                    </Carousel.Item>
                </Carousel>
            </Container>
        </React.Fragment>
    )
};

export default About;
