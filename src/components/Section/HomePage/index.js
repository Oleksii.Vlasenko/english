import React from "react";
import {Button, Carousel, Col, Container, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

const HomePage = () => {
    const carouselContent = [
        {
            title: "JUST ENGLISH",
            text: "Create and manage your own groups. Together we will test your students` knowledge!",
            link: "/about"
        },
        {
            title: "GRAMMAR TASKS",
            text: "We have a lot of grammar tasks. Increase your knowledge as often as possible.",
            link: "/practice/grammar"
        },
        {
            title: "VOCABULARY TASKS",
            text: "We have something to teach you! Increase your vocabulary every day.",
            link: "/practice/vocabulary"
        }
    ];

    const carouselItems = () => {
        return carouselContent.map((item, index) =>
            <Carousel.Item key={index}>
                <div className="">
                    <h1 className="text-dark-blue text-uppercase font-weight-bold mx-4">
                        {carouselContent[index].title}
                    </h1>
                    <p className="font-weight-bold mx-4">
                        {carouselContent[index].text}
                    </p>
                    <Link to={`${carouselContent[index].link}`}>
                        <Button variant="danger"
                                size="lg"
                                className="mt-5 px-5 text-uppercase rounded-pill">
                            {index === 0 ? "Read more" : "Try now"}
                        </Button>
                    </Link>
                </div>
            </Carousel.Item>)
    };

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component homepage">
                <Row lg="2" md="1" className="h-100 m-0">
                    <Col className="h-100 text-center d-flex flex-column justify-content-center p-0">
                        <Carousel className="w-100 h-50 main-carousel">
                            {carouselItems()}
                        </Carousel>
                    </Col>
                </Row>
                {/*<a href="https://www.facebook.com/" className="d-none d-lg-inline-block d-md-inline-block">*/}
                {/*    <FontAwesomeIcon icon={faFacebookF} className="homepage-contacts item1"/>*/}
                {/*</a>*/}
                {/*<a href="https://twitter.com/" className="d-none d-lg-inline-block d-md-inline-block">*/}
                {/*    <FontAwesomeIcon icon={faTwitter} className="homepage-contacts item2"/>*/}
                {/*</a>*/}
                {/*<a href="https://web.telegram.org/" className="d-none d-lg-inline-block d-md-inline-block">*/}
                {/*    <FontAwesomeIcon icon={faTelegramPlane} className="homepage-contacts item3"/>*/}
                {/*</a>*/}
                {/*<a href="https://mail.google.com/" className="d-none d-lg-inline-block d-md-inline-block">*/}
                {/*    <FontAwesomeIcon icon={faAt} className="homepage-contacts item4"/>*/}
                {/*</a>*/}
                {/*<a href="https://www.facebook.com/" className="d-lg-none d-md-none">*/}
                {/*    <FontAwesomeIcon icon={faFacebookF} className="homepage-contacts-2 item1"/>*/}
                {/*</a>*/}
                {/*<a href="https://twitter.com/" className="d-lg-none d-md-none">*/}
                {/*    <FontAwesomeIcon icon={faTwitter} className="homepage-contacts-2 item2"/>*/}
                {/*</a>*/}
                {/*<a href="https://web.telegram.org/" className="d-lg-none d-md-none">*/}
                {/*    <FontAwesomeIcon icon={faTelegramPlane} className="homepage-contacts-2 item3"/>*/}
                {/*</a>*/}
                {/*<a href="https://mail.google.com/" className="d-lg-none d-md-none">*/}
                {/*    <FontAwesomeIcon icon={faAt} className="homepage-contacts-2 item4"/>*/}
                {/*</a>*/}
            </Container>
        </React.Fragment>
    )
};

export default HomePage;
