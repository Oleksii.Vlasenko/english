import React, {useState} from "react";
import {Button, ButtonGroup, Container, Dropdown, DropdownButton} from "react-bootstrap";
import {useSelector} from "react-redux";
import AddVocabularyTask from "../../Staffroom/StaffroomTasks/StaffroomTaskGroups/StaffroomAddTask/AddVocabularyTask";
import StandardVocabularyTasksList from "./StandardVocabularyTasksList";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StandardVocabularyTasks = () => {

    const {vocabularyThemes, vocabularySubthemes} = useSelector(state => state);
    const {vocabularyThemesList} = vocabularyThemes;
    const {vocabularySubthemesList} = vocabularySubthemes;

    const [subthemeId, setSubthemeId] = useState(null);
    const [subthemeTitle, setSubthemeTitle] = useState(null);
    const [show, setShow] = useState(false);

    return (
        <React.Fragment>
            <Container fluid
                       className="m-0 p-0 container-fluid-component task"
            >
                <Container className="p-0">
                    <h2 className="text-center font-italic font-weight-bold text-dark-blue py-4 font-dancing-script">
                        {subthemeTitle ? `${subthemeTitle}` : "Please, choose a theme"}
                    </h2>
                    <Button disabled={!subthemeId}
                            onClick={() => setShow(true)}
                            className="bg-light-blue"
                    >
                        <FontAwesomeIcon icon={faPlus} className="mx-5"/>
                    </Button>
                    <div className="d-flex justify-content-between my-3">
                        {vocabularyThemesList &&
                        vocabularyThemesList.map((item, index) => <DropdownButton
                            as={ButtonGroup}
                            id={`dropdown-variants-primary`}
                            key={index}
                            title={item.title}
                            size="sm"
                            className="mx-1 bg-light-blue"
                        >
                            {vocabularySubthemesList &&
                            vocabularySubthemesList
                                .filter(subItem => subItem.theme === item._id)
                                .map((subItem, index) =>
                                    <Dropdown.Item eventKey={subItem.title}
                                                   key={index}
                                                   onClick={() => {
                                                       setSubthemeId(subItem._id);
                                                       setSubthemeTitle(subItem.title);
                                                   }}
                                    >
                                        {subItem.title}
                                    </Dropdown.Item>)
                            }
                        </DropdownButton>)
                        }
                    </div>
                    {subthemeId && <StandardVocabularyTasksList taskGroupId={subthemeId}/>}
                    <AddVocabularyTask show={show}
                                       setShow={setShow}
                                       taskGroupId={subthemeId}/>
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default StandardVocabularyTasks;
