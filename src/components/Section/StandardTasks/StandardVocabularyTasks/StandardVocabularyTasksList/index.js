import React, {useEffect, useState} from "react";
import {Alert, Button, Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMinusSquare, faPenFancy} from "@fortawesome/free-solid-svg-icons";
import {editVocabularyTaskById} from "../../../../../store/actions/vocabularyTask.action";
import {useDispatch, useSelector} from "react-redux";
import {getVocabularyTaskById} from "../../../../../services/vocabularyTasks.service";
import C from "../../../../../store/consts";

const StandardVocabularyTasksList = ({taskGroupId}) => {

    const dispatch = useDispatch();
    const {token, id} = useSelector(state => state.auth.user);

    const [tasks, setTasks] = useState(null);
    const [show, setShow] = useState(false);
    const [alertMessage, setAlertMessage] = useState("");

    const hideAlert = () => {
        setShow(false);
    };

    useEffect(() => {
        (async function () {
            try {
                dispatch({type: C.LOADING_START});
                await getVocabularyTaskById(taskGroupId)
                    .then(result => setTasks(result.data));
                dispatch({type: C.LOADING_END});
            } catch (e) {
                setAlertMessage(e.message);
                setShow(true);
                setTimeout(hideAlert, 4000);
            }
        })()
    }, [taskGroupId]);

    const editTaskGroups = (item, isAddBtn) => {
        const body = isAddBtn
            ? {taskGroups: [...item.taskGroups, taskGroupId]}
            : {taskGroups: item.taskGroups.filter(item => item !== taskGroupId)};
        editVocabularyTaskById()(dispatch, token, body, item._id);
    };

    return (
        <React.Fragment>
            {show &&
            <Alert variant="danger"
                   onClose={() => setShow(false)}
                   dismissible
                   className="float-right mr-md-5 m-sm-0 alert-message"
            >
                {alertMessage}
            </Alert>}
            <Table bordered
                   striped
                   hover
                   responsive="lg"
                   className="align-middle bg-light"
            >
                <thead>
                <tr className="text-center text-uppercase text-dark-blue font-weight-bold bg-light-header">
                    <th>#</th>
                    <th>Word</th>
                    <th>Transcription</th>
                    <th>Translation</th>
                    <th>-</th>
                </tr>
                </thead>
                <tbody>
                {
                    tasks && tasks.map((item, index) =>
                        <tr key={index}>
                            <td>
                                {index + 1}
                            </td>
                            <td>
                                {item.word}
                            </td>
                            <td>
                                [ {item.transcription} ]
                            </td>
                            <td>
                                {item.translation}
                            </td>
                            <td className="text-center">
                                <Button size="sm"
                                        className="px-4 bg-light-blue"
                                        onClick={() => editTaskGroups(item, false)}
                                >
                                    <FontAwesomeIcon icon={faMinusSquare}/>
                                </Button>
                            </td>
                        </tr>)
                }
                </tbody>
            </Table>
        </React.Fragment>
    )

};

export default StandardVocabularyTasksList;
