import React, {useEffect, useState} from "react";
import {Alert, Button, Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMinusSquare, faPenFancy} from "@fortawesome/free-solid-svg-icons";
import {editGrammarTaskById} from "../../../../../store/actions/grammarTask.action";
import {useDispatch, useSelector} from "react-redux";
import {getGrammarTaskById} from "../../../../../services/grammarTasks.service";
import C from "../../../../../store/consts";

const StandardGrammarTasksList = ({taskGroupId}) => {

    const dispatch = useDispatch();
    const {token, id} = useSelector(state => state.auth.user);

    const [tasks, setTasks] = useState([]);
    const [show, setShow] = useState(false);
    const [alertMessage, setAlertMessage] = useState("");

    const hideAlert = () => {
        setShow(false);
    };

    useEffect(() => {
        (async function () {
            try {
                dispatch({type: C.LOADING_START});
                await getGrammarTaskById(taskGroupId)
                    .then(result => setTasks(result.data));
                dispatch({type: C.LOADING_END});
            } catch (e) {
                setAlertMessage(e.message);
                setShow(true);
                setTimeout(hideAlert, 4000);
            }
        })()
    }, [taskGroupId]);

    const editTaskGroups = async (item, isAddBtn) => {
        const body = isAddBtn
            ? {taskGroups: [...item.taskGroups, id]}
            : {taskGroups: item.taskGroups.filter(item => item !== id)};
        await editGrammarTaskById()(dispatch, token, body, item._id);
    };

    return (
        <React.Fragment>
            {show &&
            <Alert variant="danger"
                   onClose={() => setShow(false)}
                   dismissible
                   className="float-right mr-md-5 m-sm-0 alert-message"
            >
                {alertMessage}
            </Alert>}
            <Table bordered
                   striped
                   hover
                   responsive="lg"
                   className="align-middle bg-light"
            >
                <thead>
                <tr className="text-center text-uppercase text-dark-blue font-weight-bold bg-light-header">
                    <th>#</th>
                    <th style={{width: "80%"}}>Sentence</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    tasks.length > 0 && tasks.map((item, index) =>
                        <tr key={index}>
                            <td className="text-center">
                                {index + 1}
                            </td>
                            <td>
                                {item.firstPart} {item.answer} {item.lastPart}
                            </td>
                            <td className="text-center">
                                <Button size="sm"
                                        className="px-4 bg-light-blue"
                                        onClick={() => editTaskGroups(item, false)}>
                                    <FontAwesomeIcon icon={faMinusSquare}/>
                                </Button>
                            </td>
                        </tr>)
                }
                </tbody>
            </Table>
        </React.Fragment>
    )
};

export default StandardGrammarTasksList;
