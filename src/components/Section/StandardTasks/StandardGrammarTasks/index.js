import React, {useState} from "react";
import {Button, ButtonGroup, Container, Dropdown, DropdownButton} from "react-bootstrap";
import {useSelector} from "react-redux";
import AddGrammarTask from "../../Staffroom/StaffroomTasks/StaffroomTaskGroups/StaffroomAddTask/AddGrammarTask";
import StandardGrammarTasksList from "./StandardGrammarTasksList";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StandardGrammarTasks = () => {

    const {grammarThemes, grammarSubthemes} = useSelector(state => state);

    const [subthemeId, setSubthemeId] = useState(null);
    const [subthemeTitle, setSubthemeTitle] = useState(null);
    const [show, setShow] = useState(false);

    return (
        <React.Fragment>
            <Container fluid
                       className="m-0 p-0 container-fluid-component task"
            >
                <Container className="p-0">
                    <h2 className="text-center font-italic font-weight-bold text-dark-blue py-4 font-dancing-script">
                        {subthemeTitle ? `${subthemeTitle}` : "Please, choose a theme"}
                    </h2>
                    <Button disabled={!subthemeId}
                            onClick={() => setShow(true)}
                            className="bg-light-blue"
                    >
                        <FontAwesomeIcon icon={faPlus} className="mx-5"/>
                    </Button>
                    <div className="d-flex justify-content-between flex-wrap my-3">
                        {grammarThemes &&
                        grammarThemes.map((item, index) =>
                            <DropdownButton
                                as={ButtonGroup}
                                id={`dropdown-variants-primary`}
                                key={index}
                                title={item.title}
                                size="sm"
                                className="mx-1 bg-light-blue"
                            >
                                {grammarSubthemes &&
                                grammarSubthemes
                                    .filter(subItem => subItem.theme === item._id)
                                    .map((subItem, index) =>
                                        <Dropdown.Item eventKey={subItem.title}
                                                       key={index}
                                                       onClick={() => {
                                                           setSubthemeId(subItem._id);
                                                           setSubthemeTitle(subItem.title);
                                                       }}
                                        >
                                            {subItem.title}
                                        </Dropdown.Item>)
                                }
                            </DropdownButton>)
                        }
                    </div>
                    {subthemeId && <StandardGrammarTasksList taskGroupId={subthemeId}/>}
                    <AddGrammarTask show={show}
                                    setShow={setShow}
                                    taskGroupId={subthemeId}/>
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default StandardGrammarTasks;
