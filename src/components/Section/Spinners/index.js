import React from "react";
import {Spinner} from "react-bootstrap";

const Spinners = () => {

    const variant = "primary";
    const animation = "grow";

    return (
        <React.Fragment>
            <div className="d-flex justify-content-between">
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
                <Spinner size="sm" animation={animation} variant={variant} className="mt-5"/>
            </div>
        </React.Fragment>
    )
};

export default Spinners;
