import React, {Component} from 'react';

class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = {hasError: false};
    }

    static getDerivedStateFromError(error) {
        // Оновлюємо стан, щоб наступний рендер показав запасний UI.
        return {hasError: true};
    }

    componentDidCatch(error, errorInfo) {
        // Ви також можете передати помилку в службу звітування про помилки

    }

    render() {
        if (this.state.hasError) {
            // Ви можете відрендерити будь-який власний запасний UI
            return (<div>
                <h2>Something went wrong.</h2>
            </div>)
        }

        return this.props.children;
    }
}

export default ErrorBoundary;
