import React, {useEffect, useState} from "react";
import {Button, Card, ProgressBar} from "react-bootstrap";
import ModalTasks from "./ModalTasks";
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCalendarAlt, faPlay} from '@fortawesome/free-solid-svg-icons';

const TasksItem = ({title, taskGroupId, date, description}) => {
    const {marks} = useSelector(state => state);
    const [show, setShow] = useState(false);
    const [mark, setMark] = useState(null);
    const [dateColor, setDateColor] = useState("bg-light-blue");

    const convertDate = (expDate) => {
        if (expDate) {
            const date = new Date(Date.parse(expDate));
            return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`;
        }
        return 0;
    };

    useEffect(() => {
        if (marks.marksList.length > 0) {
            let currMark = marks.marksList.find(item => item.taskGroup === taskGroupId);
            setMark(() => currMark);
            const dateCount = new Date - new Date(Date.parse(date));
            if (dateCount > 0 && !currMark) setDateColor(() => "bg-danger");
        }
    }, [marks, taskGroupId]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <React.Fragment>
            <div className="col my-3 d-flex justify-content-center">
                <Card className="shadow-lg text-dark-blue rounded-2rem w-20rem">
                    <Card.Header className="d-block bg-light-header rounded-2rem-up">
                        <Card.Title className="m-0">
                            {title}
                        </Card.Title>
                    </Card.Header>
                    <Card.Body className="d-block"
                               style={{height: '7rem'}}>
                        <Card.Text>{description}</Card.Text>
                    </Card.Body>
                    <Card.Footer className="d-block">
                        <Button onClick={handleShow}
                                disabled={!!mark}
                                style={{width: "5rem"}}
                                className="text-white shadow bg-light-blue task-btn">
                            {mark ? `${mark.value}%` : <span><FontAwesomeIcon icon={faPlay}/> Start</span>}
                        </Button>
                        <span
                            className={`p-0 m-0 p-2 ${dateColor} rounded position-relative shadow text-light`}
                            style={{top: '-9rem', right: '-8rem'}}>
                            <FontAwesomeIcon icon={faCalendarAlt} /> {convertDate(date)}
                        </span>
                    </Card.Footer>
                </Card>
            </div>
            <ModalTasks show={show} onHide={handleClose} taskId={taskGroupId}/>
        </React.Fragment>
    )
};

export default TasksItem;
