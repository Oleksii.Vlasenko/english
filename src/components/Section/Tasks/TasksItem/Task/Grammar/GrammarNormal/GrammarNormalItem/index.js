import React, {useEffect, useState} from "react";
import {Button, Card, CardGroup, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

const GrammarNormalItem = ({time, action, num, length, item}) => {
    const {firstPart, lastPart, answer, translation} = item;

    const [answers, setAnswers] = useState([]);
    const [selectAnswers, setSelectAnswers] = useState([]);

    const [timeLeft, setTimeLeft] = useState(time);

    useEffect(() => {

        if (timeLeft < 0.01) {
            checkAnswer();
            return;
        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 0.01);
        }, 10);

        return () => clearInterval(intervalId);

    }, [timeLeft]);

    const isEquals = (arr1, arr2) => {
        if (arr1.length !== arr2.length) {
            return false;
        }
        return arr1.join("") === arr2.join("");
    };

    const shuffle = (arr) => {
        let index = arr.length;
        while (index) {
            index--;
            let randomIndex = Math.floor(Math.random() * index);
            [arr[index], arr[randomIndex]] = [arr[randomIndex], arr[index]];
        }
        return arr;
    };

    const addAnswer = (event) => {
        setSelectAnswers([...selectAnswers, event.target.innerHTML]);
        setAnswers(answers.filter(item => item !== event.target.innerHTML));
    };

    const remAnswer = (event) => {
        setAnswers([...answers, event.target.innerHTML]);
        setSelectAnswers(selectAnswers.filter(item => item !== event.target.innerHTML));
    };

    useEffect(() => {
        setAnswers(shuffle([...firstPart.split(" "), answer, ...lastPart.split(" ")]));
    }, [item]);

    const checkAnswer = async () => {
        await setTimeLeft(time);
        action(isEquals(selectAnswers, [...firstPart.split(" "), answer, ...lastPart.split(" ")]));
        setSelectAnswers([]);
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up p-0 pt-3">
                                <span className="p-3">{num} / {length}</span>
                                <div style={{height: "5px", backgroundColor: "#F7FB66", width: `${100 * ((time - timeLeft) / time)}%`}}
                                     className="mt-3" />
                            </Card.Header>
                            <Card.Body>
                                <Card.Title className="pb-3">{translation}</Card.Title>
                                <div style={{minHeight: "8rem"}}>
                                    {selectAnswers.length > 0
                                    && selectAnswers.map((item, index) =>
                                        <Button variant="light" key={index} className="m-1" onClick={remAnswer}>
                                            {item}
                                        </Button>)
                                    }
                                </div>
                                <div style={{minHeight: "8rem"}}>
                                    {answers.length > 0
                                    && answers.map((item, index) =>
                                        <Button key={index} variant="secondary" className="m-1" onClick={addAnswer}>
                                            {item}
                                        </Button>)
                                    }
                                </div>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue"
                                >
                                    <FontAwesomeIcon icon={faArrowRight} className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default GrammarNormalItem;
