import React, {useState} from "react";
import {Button, Card, CardGroup, Col, Form, FormControl, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

const GrammarHardItem = ({action, num, length, item}) => {
    const {firstPart, lastPart, answer, infinitive} = item;

    const [selectAnswer, setSelectAnswer] = useState("");

    const changeHandler = (event) => {
        setSelectAnswer(event.target.value);
    };

    const checkAnswer = () => {
        action(selectAnswer === answer.trim());
        setSelectAnswer("");
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up">
                                <span className="py-3">{num} / {length}</span>
                            </Card.Header>
                            <Card.Body>
                                <Card.Text>{firstPart} ________ {lastPart} ({infinitive})</Card.Text>
                                <Form onChange={changeHandler} className="pt-5">
                                    <FormControl aria-label="Text input with radio button"
                                                 placeholder={infinitive}
                                                 value={selectAnswer}
                                    />
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue">
                                    <FontAwesomeIcon icon={faArrowRight} className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default GrammarHardItem;
