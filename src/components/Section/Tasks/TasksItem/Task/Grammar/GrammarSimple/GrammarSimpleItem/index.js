import React, {useEffect, useState} from "react";
import {Button, Card, CardGroup, Col, Form, FormControl, InputGroup, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";


const GramPracSimpleItem = ({time, action, num, length, item}) => {
    const {firstPart, lastPart, answer, options} = item;

    const [answers, setAnswers] = useState([]);

    const [radio, setRadio] = useState(null);

    const [timeLeft, setTimeLeft] = useState(time);

    useEffect(() => {

        if (timeLeft < 0.005) {
            checkAnswer();
            return;
        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 0.005);
        }, 5);

        return () => clearInterval(intervalId);

    }, [timeLeft]);

    const shuffle = (arr) => {
        let index = arr.length;
        while (index) {
            index--;
            let randomIndex = Math.floor(Math.random() * index);
            [arr[index], arr[randomIndex]] = [arr[randomIndex], arr[index]];
        }
        return arr;
    };

    useEffect(() => {
        setAnswers(() => shuffle([...options, answer]));
        setRadio(null);
    }, [options, answer]);

    const changeRadio = (item) => {
        setRadio(item);
    };

    const checkAnswer = async () => {
        await setTimeLeft(time);
        action(answers[radio] === answer);
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up p-0 pt-3">
                                    <span className="p-3">{num} / {length}</span>
                                <div style={{height: "5px", backgroundColor: "#F7FB66", width: `${100 * ((time - timeLeft) / time)}%`}}
                                     className="mt-3" />
                            </Card.Header>
                            <Card.Body>
                                <Card.Text className="pb-3">
                                    {firstPart} ________ {lastPart}
                                </Card.Text>
                                <Form>
                                    {answers
                                    && answers.map((item, index) =>
                                        <div onClick={() => changeRadio(index)} key={index}>
                                            <InputGroup className="py-1">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Radio
                                                        checked={radio === index}
                                                        onClick={() => changeRadio(index)}
                                                        aria-label="Radio button for following text input"
                                                        name="formHorizontalRadios"
                                                        id={item}/>
                                                </InputGroup.Prepend>
                                                <FormControl aria-label="Text input with radio button"
                                                             disabled={true}
                                                             value={`${item}`}/>
                                            </InputGroup>
                                        </div>)}
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue"
                                >
                                    <FontAwesomeIcon icon={faArrowRight}
                                                     className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default GramPracSimpleItem;
