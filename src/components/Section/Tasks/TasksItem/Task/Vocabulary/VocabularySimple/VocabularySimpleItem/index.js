import React, {useEffect, useState} from "react";
import {Button, Card, CardGroup, Col, Form, FormControl, InputGroup, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

const VocabularySimpleItem = ({item, num, action, length, time}) => {
    const {word, translation, options} = item;

    const [selectAnswer, setSelectAnswer] = useState(null);

    const [radio, setRadio] = useState(null);

    const [timeLeft, setTimeLeft] = useState(time);

    useEffect(() => {

        if (timeLeft < 0.005) {
            checkAnswer();
            return;
        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 0.005);
        }, 5);

        return () => clearInterval(intervalId);

    }, [timeLeft]);

    useEffect(() => {
        setRadio(null);
    }, [options]);

    const changeRadio = (item) => {
        setSelectAnswer(options[item]);
        setRadio(item);
    };

    const checkAnswer = async () => {
        await setTimeLeft(time);
        action(selectAnswer === translation.trim());
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up p-0 pt-3">
                                <span className="p-3">{num} / {length}</span>
                                <div style={{height: "5px", backgroundColor: "#F7FB66", width: `${100 * ((time - timeLeft) / time)}%`}}
                                     className="mt-3" />
                            </Card.Header>
                            <Card.Body>
                                <Card.Title className="pb-3">
                                    {word}
                                </Card.Title>
                                <Form>
                                    {options
                                    && options.map((item, index) =>
                                        <div onClick={() => changeRadio(index)} key={index}>
                                            <InputGroup className="py-1">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Radio
                                                        checked={radio === index}
                                                        aria-label="Radio button for following text input"
                                                        name="formHorizontalRadios"
                                                        id={item}/>
                                                </InputGroup.Prepend>
                                                <FormControl aria-label="Text input with radio button"
                                                             disabled={true}
                                                             value={`${item}`}/>
                                            </InputGroup>
                                        </div>)}
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue"
                                >
                                    <FontAwesomeIcon icon={faArrowRight}
                                                     className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default VocabularySimpleItem;
