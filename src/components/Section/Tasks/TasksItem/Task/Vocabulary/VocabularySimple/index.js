import React, {useEffect, useState} from "react";
import VocabularySimpleItem from "./VocabularySimpleItem";
import {Button, Card, CardGroup, Col, Container, Row} from 'react-bootstrap';
import {useDispatch, useSelector} from "react-redux";
import {editMarkById} from "../../../../../../../store/actions/mark.action";
import {getVocabularyTaskById} from "../../../../../../../services/vocabularyTasks.service";
import C from "../../../../../../../store/consts";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";


const VocabularySimple = ({mark, taskGroup, time}) => {

    const {token} = useSelector(state => state.auth.user);

    const [tasks, setTasks] = useState([]);
    const [index, setIndex] = useState(0);
    const [correctAnswers, setCorrectAnswers] = useState(0);

    const dispatch = useDispatch();

    const [startTimer, setStartTimer] = useState(3000);

    const timer = () => {
        setStartTimer(startTimer - 1000);
    };

    const createOptions = (answer, tasks) => {
        let options = [];
        options.push(answer);
        let count = 3;
        while (count) {
            const randIndex = Math.floor(Math.random() * tasks.length);
            if (!options.includes(tasks[randIndex].translation)) {
                options.push(tasks[randIndex].translation);
                count--;
            }
        }
        return shuffle(options);
    };

    useEffect(() => {
        if (tasks.length > 0) {
            if (!tasks[0].options) {
                setTasks(tasks.map(item => (
                    {
                        ...item,
                        options: createOptions(item.translation, tasks)
                    })));
            }
        }
    }, [tasks]);

    useEffect(() => {
        (async () => {
            dispatch({type: C.LOADING_START});
            await getVocabularyTaskById(taskGroup)
                .then(result => {
                    setTasks(shuffle(result.data))
                });
            dispatch({type: C.LOADING_END});
        })();
    }, [taskGroup]);

    useEffect(() => {
        if (startTimer >= 0) {
            setTimeout(timer, 1000);
        }
    }, [startTimer]);

    const checkAnswer = async (answer) => {
        if (answer) {
            setCorrectAnswers(() => correctAnswers + 1);
        }
        setIndex(() => index + 1);
    };

    const sendResult = async () => {
        const value = Math.round(correctAnswers / tasks.length * 100);
        await editMarkById()(dispatch, token, {value}, mark._id);
    };

    const shuffle = (arr) => {
        let index = arr.length;
        while (index) {
            index--;
            let randomIndex = Math.floor(Math.random() * index);
            [arr[index], arr[randomIndex]] = [arr[randomIndex], arr[index]];
        }
        return arr;
    };

    useEffect(() => {
        if (index === tasks.length && index !== 0 && mark) {
            sendResult();
            setIndex(() => index + 1);
        }
    });

    return (
        <React.Fragment>
            <Container className="mt-5 pt-sm-0 pt-md-5 ">
                {
                    startTimer >= 0
                    && <h1 className="text-center text-danger">
                        {startTimer === 0 ? "START" : startTimer / 1000}
                    </h1>
                }
                {startTimer < 0
                && tasks.length > 0
                && tasks.length > index
                && <VocabularySimpleItem action={checkAnswer}
                                         item={tasks[index]}
                                         num={index + 1}
                                         length={tasks.length}
                                         time={time}
                />}
                {
                    tasks.length > 0
                    && tasks.length < index
                    && <Row>
                        <Col xs={8} lg={4} className="mx-auto">
                            <CardGroup>
                                <Card className="rounded-2rem">
                                    <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up">
                                        <h4 className="text-uppercase">result</h4>
                                    </Card.Header>
                                    <Card.Body>
                                        <h5>
                                            Your mark is {Math.round(correctAnswers / tasks.length * 100)}
                                        </h5>
                                    </Card.Body>
                                    <Card.Footer>
                                        <Link to="/tasks">
                                            <Button className="px-4 float-right bg-light-blue">
                                                <FontAwesomeIcon icon={faCheck} className="text-light"/>
                                            </Button>
                                        </Link>
                                    </Card.Footer>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                }
            </Container>
        </React.Fragment>
    )
};

export default VocabularySimple;
