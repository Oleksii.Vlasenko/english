import React, {useEffect, useState} from "react";
import {Button, Card, CardGroup, Col, Form, FormControl, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

const VocabularyNormalItem = ({item, num, action, length, time}) => {

    const {word, translation} = item;

    const [selectAnswer, setSelectAnswer] = useState(null);

    const [timeLeft, setTimeLeft] = useState(time);

    useEffect(() => {

        if (timeLeft < 0.005) {
            checkAnswer();
            return;
        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 0.005);
        }, 5);

        return () => clearInterval(intervalId);

    }, [timeLeft]);

    const changeHandler = (event) => {
        setSelectAnswer(event.target.value);
    };

    const checkAnswer = async () => {
        await setTimeLeft(time);
        action(selectAnswer === word.trim());
        await setSelectAnswer("");
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up p-0 pt-3">
                                <span className="p-3">{num} / {length}</span>
                                <div style={{height: "5px", backgroundColor: "#F7FB66", width: `${100 * ((time - timeLeft) / time)}%`}}
                                     className="mt-3" />
                            </Card.Header>
                            <Card.Body>
                                <Card.Title className="pb-3">
                                    {translation}
                                </Card.Title>
                                <Form onChange={changeHandler} className="pt-5">
                                    <FormControl aria-label="Text input with radio button"
                                                 value={selectAnswer}
                                    />
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue"
                                >
                                    <FontAwesomeIcon icon={faArrowRight}
                                                     className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default VocabularyNormalItem;
