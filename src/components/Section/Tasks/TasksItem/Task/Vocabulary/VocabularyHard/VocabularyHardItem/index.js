import React, {useEffect, useState} from "react";
import {Col, Row} from "react-bootstrap";

const VocabularyHardItem = ({item, action, time}) => {

    const [timeLeft, setTimeLeft] = useState(time);

    useEffect(() => {

        if (timeLeft < 0.005) {
            checkAnswer();
            return;
        }

        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 0.005);
        }, 5);

        return () => clearInterval(intervalId);

    }, [timeLeft]);

    const checkAnswer = async (elem) => {
        await setTimeLeft(time);
        action(elem === item.word);
    };

    return (
        <React.Fragment>
            <div style={{height: "5px", backgroundColor: "#ff2222", width: `${100 * ((time - timeLeft) / time)}%`}}/>
            <Row className="justify-content-center m-0 p-0 mt-5" style={{minHeight: "40%"}}>
                <Col md="6" sm="8" xs="12" className="p-0 m-0" style={{border: "none"}}>
                    <div
                        className="border-0 h-50 py-5 bg-middle-blue m-2 rounded-2rem d-flex justify-content-center align-items-center"
                    >
                        <h2 className="py-5 text-light">{item.translation}</h2>
                    </div>
                </Col>
            </Row>
            <Row className="" style={{minHeight: "50%"}}>
                {item && item.options
                && item.options.map((item, index) =>
                    <Col md="3" sm="12" className="" key={index}>
                        <div
                            className="border-0 h-75 bg-light-blue m-1 rounded-2rem d-flex justify-content-center align-items-center"
                            onClick={() => checkAnswer(item)}
                        >
                            <h2>{item}</h2>
                        </div>
                    </Col>)}
            </Row>
        </React.Fragment>
    )
};

export default VocabularyHardItem;
