import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import VocabularyHard from "./Vocabulary/VocabularyHard";
import VocabularyNormal from "./Vocabulary/VocabularyNormal";
import VocabularySimple from "./Vocabulary/VocabularySimple";
import GrammarSimple from "./Grammar/GrammarSimple";
import GrammarNormal from "./Grammar/GrammarNormal";
import GrammarHard from "./Grammar/GrammarHard";
import {addMark} from "../../../../../store/actions/mark.action";


const Task = () => {
    const {taskGroups, auth, marks} = useSelector(state => state);
    const {taskGroupsList} = taskGroups;
    const {token} = auth.user;
    const {marksList} = marks;

    const [taskGroup, setTaskGroup] = useState(null);
    const [mark, setMark] = useState(null);

    const dispatch = useDispatch();

    const id = window.location.href.split('?id=')[1];

    useEffect(() => {
        setTaskGroup(taskGroupsList.find(item => item._id === id));
    }, [id, taskGroupsList]);

    useEffect(() => {
        (async function () {
            await addMark()(dispatch, token, {taskGroup: id, value: 0});
        })();
    }, [id, dispatch, token]);

    useEffect(() => {
        if (marksList.length > 0) {
            setMark(() => marksList.find(item => item.taskGroup === id));
        }
    }, [id, marksList]);

    const getTasks = () => {
        if (taskGroup) {
            const taskSelector = `${taskGroup.taskType}-${taskGroup.difficulty}`;
            switch (taskSelector) {
                case 'grammar-easy':
                    return <GrammarSimple mark={mark}
                                          taskGroup={id}
                                          time={taskGroup.time}/>;
                case 'grammar-normal':
                    return <GrammarNormal mark={mark}
                                          taskGroup={id}
                                          time={taskGroup.time}/>;
                case 'grammar-hard':
                    return <GrammarHard mark={mark}
                                        taskGroup={id}
                                        time={taskGroup.time}/>;
                case 'vocabulary-easy':
                    return <VocabularySimple mark={mark}
                                             taskGroup={id}
                                             time={taskGroup.time}/>;
                case 'vocabulary-normal':
                    return <VocabularyNormal mark={mark}
                                             taskGroup={id}
                                             time={taskGroup.time}/>;
                case 'vocabulary-hard':
                    return <VocabularyHard mark={mark}
                                           taskGroup={id}
                                           time={taskGroup.time}/>;
                default:
                    return <h1>Unknown task!</h1>
            }
        }
    };

    return (
        <React.Fragment>
            <Container fluid className="container-fluid-component task p-0">
                {taskGroup && getTasks()}
            </Container>
        </React.Fragment>
    )
};

export default Task;
