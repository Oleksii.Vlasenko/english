import React, {useEffect, useState} from "react";
import {Button, Modal} from 'react-bootstrap'
import {Link} from "react-router-dom";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useSelector} from "react-redux";

const ModalTasks = ({taskId, show, onHide}) => {

    const {taskGroupsList} = useSelector(state => state.taskGroups);

    const [taskGroup, setTaskGroup] = useState(null);

    useEffect(() => {
        setTaskGroup(taskGroupsList.find(item => item._id === taskId));
    }, [taskId]);

    const description = (item) => {
        const taskSelector = `${item.taskType}-${item.difficulty}`;
        switch (taskSelector) {
            case 'grammar-easy':
                return "choose the answer from several suggested";
            case 'grammar-normal':
                return "make a sentence from the suggested words";
            case 'grammar-hard':
                return "enter the answer yourself";
            case 'vocabulary-easy':
                return "choose the correct translation of the English word";
            case 'vocabulary-normal':
                return "enter the answer yourself";
            case 'vocabulary-hard':
                return "choose the correct translation of the Ukrainian word";
            default:
                return "";
        }
    };

    return (
        <React.Fragment>
            <Modal show={show}
                   onHide={onHide}
                   className="rounded-modal"
            >
                <Modal.Header closeButton className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title>confirm your choose</Modal.Title>
                </Modal.Header>
                <Modal.Body className="py-5">
                    {taskGroup
                        ?
                        <div>
                            <p>This is the {taskGroup.taskType} task.</p>
                            <p>You have {taskGroup.time} seconds for 1 question.</p>
                            <p>You should {description(taskGroup)}.</p>
                        </div>
                        : " "}
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between rounded-2rem-down bg-light-header">
                    <Link to={`/task?id=${taskId}`}>
                        <Button className="px-5 text-light text-uppercase font-weight-bold bg-light-blue">
                            <FontAwesomeIcon icon={faCheck}/>
                        </Button>
                    </Link>
                    <Button onClick={onHide}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue">
                        <FontAwesomeIcon icon={faTimes}/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    );
};

export default ModalTasks
