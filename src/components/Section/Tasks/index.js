import React, {useEffect} from "react";
import {Container} from 'react-bootstrap';
import TasksItem from "./TasksItem";
import {useDispatch, useSelector} from "react-redux";
import {saveMarksByTaskGroup} from "../../../store/actions/mark.action";
import {saveStudentTaskGroups} from "../../../store/actions/taskGroup.action";

const Tasks = () => {

    const {taskGroups, auth, groups} = useSelector(state => state);
    const {taskGroupsList, isLoading} = taskGroups;
    const {token, role, id} = auth.user;
    const {groupsList} = groups;
    const dispatch = useDispatch();

    useEffect(() => {
        if (groupsList && role === "student") {
            saveStudentTaskGroups()(dispatch, token, id);
        }

    }, [groupsList, id, role, dispatch, token]);

    // useEffect(() => {
    //     if (role !== "student" && taskGroupsList.length > 0) {
    //         taskGroupsList.forEach(item =>
    //             saveMarksByTaskGroup()(dispatch, token, item._id));
    //     }
    // }, [dispatch, token, taskGroupsList, role]);

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component task"
            >
                <Container style={{filter: 'none'}}>
                    <div className="py-3">
                        {!isLoading &&
                        <h2 className="text-center font-weight-bold text-dark-blue font-dancing-script">
                            Your tasks
                        </h2>}
                    </div>
                    <div className="row row-cols-xl-3 row-cols-md-2 row-cols-sm-1">
                        {taskGroupsList.length > 0 && taskGroupsList.map((item, index) =>
                            <TasksItem key={index}
                                       title={item.title}
                                       taskGroupId={item._id}
                                       date={item.expDate}
                                       description={item.description}
                                       taskType={item.taskType}
                            />
                        )}
                    </div>
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default Tasks;
