import React, {useEffect, useState} from "react";
import SectionRoutes from "../../routes/index";
import {useDispatch, useSelector} from "react-redux";
import {saveThemes, saveSubthemes} from "../../store/actions";
import {saveGroups} from "../../store/actions/group.action";
import {saveTaskGroups} from "../../store/actions/taskGroup.action";
import {saveMarks} from "../../store/actions/mark.action";
import {saveVocabularyThemes} from "../../store/actions/vocabularyTheme.action";
import {saveVocabularySubthemes} from "../../store/actions/vocabularySubtheme.action";
import {Container} from "react-bootstrap";

const Section = () => {
    const {token, role} = useSelector(state => state.auth.user);

    const [height, setHeight] = useState(0);

    useEffect(() => {
        setHeight(() => document.getElementById('main-header').clientHeight);
    }, []);

    let dispatch = useDispatch();

    useEffect(() => {
        saveThemes()(dispatch);
        saveSubthemes()(dispatch);
        saveVocabularyThemes()(dispatch);
        saveVocabularySubthemes()(dispatch);
        if (token) {
            if(role === "teacher" || role === "admin") saveGroups()(dispatch, token);
            if (role === "student") {
                saveMarks()(dispatch, token)
            } else {
                saveTaskGroups()(dispatch, token);
            }
        }
    }, [token, dispatch, role]);

    return (
        <React.Fragment>
            <Container fluid className="p-0 m-0" style={{height: `calc(100vh - ${height}px)`}}>
                <SectionRoutes/>
            </Container>
        </React.Fragment>
    )
};

export default Section;
