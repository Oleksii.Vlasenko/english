import React, {useEffect, useState} from "react";
import VocPracSimpleItem from "./VocPracSimpleItem";
import {Container, Row} from "react-bootstrap";
import {useSelector} from "react-redux";
import {getVocabularyTaskById} from "../../../../../services/vocabularyTasks.service";
import AlertEvent from "../../../../AlertEvent";


const VocPracSimple = () => {
    const {vocabularySubthemesList} = useSelector(state => state.vocabularySubthemes);

    const id = window.location.href.split('?id=')[1];
    // const id = "5ee742e427857b3b4c139396";

    const [list, setList] = useState([]);
    const [title, setTitle] = useState("");
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    useEffect(() => {
        if (vocabularySubthemesList.length > 0) {
            setTitle(() => vocabularySubthemesList.find(item => item._id === id).title);
        }
    }, [id, vocabularySubthemesList]);

    useEffect(() => {
        (async () => {
            try {
                await getVocabularyTaskById(id)
                    .then(result => setList(result.data));
            } catch (e) {
                setAlertForm({message: e.message, variant: "danger"});
                setShow(true);
            }
        })()
    }, [id]);

    return (
        <React.Fragment>
            <Container fluid className="p-0 m-0 container-fluid-component task">
                <Container className="p-0">
                    {show &&
                    <AlertEvent variant={alertForm.variant}
                                hide={() => setShow(false)}
                                message={alertForm.message} />}
                    <h2 className="font-dancing-script text-dark-blue text-center py-3">
                        {title}
                    </h2>
                    <Row id="voc_prac_simpl_wrapper" lg="3" md="2" xs="1">
                        {list.length > 0 && list.map((item, index) =>
                            <VocPracSimpleItem word={item.word}
                                               transcription={item.transcription}
                                               translation={item.translation}
                                               key={index}/>)}
                    </Row>
                </Container>
            </Container>
        </React.Fragment>
    )
};


export default VocPracSimple;
