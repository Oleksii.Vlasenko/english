import React from "react";
import Card from 'react-bootstrap/Card'
import {Col} from "react-bootstrap";


const VocPracSimpleItem = ({word, transcription, translation}) => {

    return (
        <React.Fragment>
            <Col className="p-3">
                <Card id="voc_prac_simpl_card"
                      className="d-inline-block text-center position-relative bg-transparent border-0 w-100"
                      style={{height: '13rem'}}>
                    <Card.Body id="voc_prac_simpl_back" className="rounded-2rem bg-light-blue">
                        <Card.Title>
                            {translation}
                        </Card.Title>
                    </Card.Body>
                    <Card.Body id="voc_prac_simpl_front" className="rounded-2rem bg-light-blue">
                        <Card.Title>{word}</Card.Title>
                        <Card.Text>
                            {transcription}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </React.Fragment>
    )
};


export default VocPracSimpleItem;
