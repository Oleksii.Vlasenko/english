import React, {useEffect, useState} from "react";
import {Button, Card, CardGroup, Col, Form, FormControl, InputGroup, Row} from 'react-bootstrap'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";


const VocPracNormalItem = ({item, num, action, length}) => {

    const {word, translation, options} = item;

    const [selectAnswer, setSelectAnswer] = useState(null);

    const [radio, setRadio] = useState(null);

    useEffect(() => {
        setRadio(null);
    }, [options]);

    const changeRadio = (item) => {
        setSelectAnswer(options[item]);
        setRadio(item);
    };

    const checkAnswer = () => {
        action(selectAnswer === translation.trim());
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up py-3">
                                <span className="p-3">{num} / {length}</span>
                                <Link to="/practice/vocabulary">
                                    <FontAwesomeIcon icon={faTimesCircle}
                                                     className="text-light float-right mt-1 text-decoration-none"/>
                                </Link>
                            </Card.Header>
                            <Card.Body>
                                <Card.Title className="pb-3 text-center">
                                    {word}
                                </Card.Title>
                                <Form>
                                    {options
                                    && options.map((item, index) =>
                                        <div onClick={(event) => changeRadio(index)}>
                                            <InputGroup key={index}
                                                        className="py-1">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Radio
                                                        checked={radio === index}
                                                        aria-label="Radio button for following text input"
                                                        name="formHorizontalRadios"
                                                        id={item}/>
                                                </InputGroup.Prepend>
                                                <FormControl aria-label="Text input with radio button"
                                                             disabled={true}
                                                             value={`${item}`}/>
                                            </InputGroup>
                                        </div>)}
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue"
                                >
                                    <FontAwesomeIcon icon={faArrowRight}
                                                     className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default VocPracNormalItem;
