import React, {useEffect, useState} from "react";
import VocPracNormalItem from "./VocPracNormalItem";
import {Button, Card, CardGroup, Col, Container, Row} from 'react-bootstrap';
import {useSelector} from "react-redux";
import AlertEvent from "../../../../AlertEvent";
import {getVocabularyTaskById} from "../../../../../services/vocabularyTasks.service";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faTimesCircle} from "@fortawesome/free-solid-svg-icons";


const VocPracNormal = () => {
    const {vocabularySubthemesList} = useSelector(state => state.vocabularySubthemes);

    const id = window.location.href.split('?id=')[1];
    // const id = "5ee742e427857b3b4c139396";

    const [tasks, setTasks] = useState([]);
    const [index, setIndex] = useState(0);
    const [correctAnswers, setCorrectAnswers] = useState(0);
    const [result, setResult] = useState([]);
    const [title, setTitle] = useState("");
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    useEffect(() => {
        if (vocabularySubthemesList.length > 0) {
            setTitle(() => vocabularySubthemesList.find(item => item._id === id).title);
        }
    }, [id, vocabularySubthemesList]);

    const createOptions = (answer, tasks) => {
        let options = [];
        options.push(answer);
        let count = 3;
        while (count) {
            const randIndex = Math.floor(Math.random() * tasks.length);
            if (!options.includes(tasks[randIndex].translation)) {
                options.push(tasks[randIndex].translation);
                count--;
            }
        }
        return shuffle(options);
    };

    useEffect(() => {
        if (tasks.length > 0) {
            if (!tasks[0].options) {
                setTasks(tasks.map(item => (
                    {
                        ...item,
                        options: createOptions(item.translation, tasks)
                    })));
            }
        }
    }, [tasks]);

    useEffect(() => {
        (async () => {
            try {
                await getVocabularyTaskById(id)
                    .then(result => setTasks(shuffle(result.data).slice(0, 20))); //slice(0, 20)
            } catch (e) {
                setAlertForm({message: e.message, variant: "danger"});
                setShow(true);
            }
        })();
    }, [id]);

    const checkAnswer = (answer) => {
        setResult([...result, !!answer]);
        if (answer) {
            setCorrectAnswers(() => correctAnswers + 1);
        }
        setIndex(() => index + 1);
    };

    const shuffle = (arr) => {
        let index = arr.length;
        while (index) {
            index--;
            let randomIndex = Math.floor(Math.random() * index);
            [arr[index], arr[randomIndex]] = [arr[randomIndex], arr[index]];
        }
        return arr;
    };

    return (
        <React.Fragment>
            <Container fluid className="p-0 m-0 container-fluid-component task">
                <Container className="">
                    {show &&
                    <AlertEvent variant={alertForm.variant}
                                hide={() => setShow(false)}
                                message={alertForm.message}/>}
                    <h2 className="font-dancing-script text-dark-blue text-center py-3">
                        {title}
                    </h2>
                    {tasks.length > 0
                    && tasks.length > index
                    && <VocPracNormalItem action={checkAnswer}
                                          item={tasks[index]}
                                          num={index + 1}
                                          length={tasks.length}
                    />}
                    {
                        tasks.length > 0
                        && tasks.length === index
                        && <Row>
                            <Col xs={12} lg={6} className="mx-auto">
                                <CardGroup>
                                    <Card className="rounded-2rem">
                                        <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up py-3">
                        <span
                            className="p-3 text-uppercase">Your mark is {Math.round(correctAnswers / tasks.length * 100)}</span>
                                            <Link to="/practice/vocabulary">
                                                <FontAwesomeIcon icon={faTimesCircle}
                                                                 className="text-light float-right mt-1 text-decoration-none"/>
                                            </Link>
                                        </Card.Header>
                                        <Card.Body>
                                            <Card.Text>
                                                <ol>
                                                    {tasks && tasks.map((item, index) =>
                                                        <li key={index}
                                                            className="text-dark-blue">
                                                            <span className="mr-3">{item.word}</span>
                                                            <span>-</span>
                                                            <span className={`${result[index] ? "text-info" : "text-danger"} mx-3 font-weight-bold`}>
                                                                {item.translation}
                                                            </span>
                                                        </li>)}
                                                </ol>
                                            </Card.Text>
                                        </Card.Body>
                                        <Card.Footer>
                                            <Link to="/practice/vocabulary" className="w-100">
                                                <Button onClick={checkAnswer}
                                                        className="px-4 float-right bg-light-blue">
                                                    <FontAwesomeIcon icon={faCheck} className="text-light"/>
                                                </Button>
                                            </Link>
                                        </Card.Footer>
                                    </Card>
                                </CardGroup>
                            </Col>
                        </Row>
                    }
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default VocPracNormal;
