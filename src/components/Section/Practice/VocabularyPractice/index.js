import React, {useState} from "react";
import {Accordion, Button, Card, Container, Modal} from "react-bootstrap";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

const VocabularyPractice = () => {
    const {vocabularyThemes, vocabularySubthemes} = useSelector(state => state);
    const {vocabularyThemesList} = vocabularyThemes;
    const {vocabularySubthemesList} = vocabularySubthemes;

    const [subthemeId, setSubthemeId] = useState("");
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);

    const handleShow = (event) => {
        setSubthemeId(event.target.id);
        setShow(true);
    };

    return (
        <React.Fragment>
            <Container fluid
                       className="m-0 p-0 container-fluid-component task"
            >
                <Container>
                    <h2 className="text-center font-weight-bold text-dark-blue py-4 font-dancing-script">
                        Vocabulary
                    </h2>
                    {vocabularyThemesList
                    && <Accordion defaultActiveKey="0"
                                  className="pt-5">
                        {vocabularyThemesList.map((item, index) =>
                            <Card id={item._id}
                                  key={index}
                                  className="mt-1 border-0 text-uppercase text-dark-blue pointer"
                                  style={{borderRadius: "1rem"}}>
                                <Accordion.Toggle as={Card.Header}
                                                  className="bg-light m-0 border-0"
                                                  eventKey={index}>
                                    <h4 className="m-0">{item.title}</h4>
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey={index}
                                                    className="text-capitalize">
                                    <ol>
                                        {
                                            vocabularySubthemesList &&
                                            vocabularySubthemesList
                                                .filter(subtheme => subtheme.theme === item._id)
                                                .map((elem, index) =>
                                                    <h4 className="my-3 ml-3"
                                                        key={index}>
                                                        <li onClick={handleShow}
                                                            id={elem._id}>{elem.title}</li>
                                                    </h4>)
                                        }
                                    </ol>
                                </Accordion.Collapse>
                            </Card>
                        )}
                    </Accordion>
                    }
                    <Modal show={show} onHide={handleClose} className="rounded-modal">
                        <Modal.Header closeButton className="text-light bg-light-blue text-uppercase rounded-2rem-up">
                            <Modal.Title>Difficulty</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>Choose the level of difficulty</Modal.Body>
                        <Modal.Footer className="d-flex justify-content-center rounded-2rem-down">
                            <div className="w-100 text-center">
                                <Link to={`/practice/vocabulary/theory/?id=${subthemeId}`} className="mx-auto">
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-4 px-xs-3 bg-light-blue task-btn">
                                        Words
                                    </Button>
                                </Link>
                            </div>
                            <div className="w-100 text-center">
                                <Link to={`/practice/vocabulary/simple/?id=${subthemeId}`} className="mx-auto">
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-4 px-xs-3 bg-light-blue task-btn">
                                        Flash cards
                                    </Button>
                                </Link>
                            </div>
                            <div className="w-100 text-center">
                                <Link to={`/practice/vocabulary/normal/?id=${subthemeId}`} className="mx-auto">
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-4 px-xs-3 bg-light-blue task-btn">
                                        Multiple choice
                                    </Button>
                                </Link>
                            </div>
                            <div className="w-100 text-center">
                                <Link to={`/practice/vocabulary/hard/?id=${subthemeId}`} className="mx-auto">
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-4 px-xs-3 bg-light-blue task-btn">
                                        Open-ended
                                    </Button>
                                </Link>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default VocabularyPractice;
