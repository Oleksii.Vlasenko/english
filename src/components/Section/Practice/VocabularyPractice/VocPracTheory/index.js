import React, {useEffect, useState} from "react";
import VocPracTheoryItem from "./VocPracTheoryItem";
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import Spinners from "../../../Spinners";
import {useSelector} from "react-redux";
import {getVocabularyTaskById} from '../../../../../services/vocabularyTasks.service'
import AlertEvent from "../../../../AlertEvent";

const VocPracTheory = () => {
    const {vocabularySubthemesList} = useSelector(state => state.vocabularySubthemes);

    const id = window.location.href.split('?id=')[1];
    // const id = "5ee742e427857b3b4c139396";

    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [title, setTitle] = useState("");
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    useEffect(() => {
        if (vocabularySubthemesList.length > 0) {
            setTitle(() => vocabularySubthemesList.find(item => item._id === id).title);
        }
    }, [id, vocabularySubthemesList]);

    useEffect(() => {
        (async () => {
            try {
                setLoading(true);
                await getVocabularyTaskById(id)
                    .then(result => setList(result.data));
                setLoading(false);
            } catch (e) {
                setAlertForm({message: e.message, variant: "danger"});
                setShow(true);
                setLoading(false);
            }
        })()
    }, [id]);

    return (
        <React.Fragment>
            <Container fluid className="p-0 m-0 container-fluid-component task">
                <Container className="p-0">
                    {show &&
                    <AlertEvent variant={alertForm.variant}
                                hide={() => setShow(false)}
                                message={alertForm.message} />}
                    <h2 className="font-dancing-script text-dark-blue text-center py-3">
                        {title}
                    </h2>
                    {loading && <Spinners/>}
                    <Table striped bordered hover className="font-italic bg-light">
                        <thead className="bg-light-blue text-center text-light">
                        <tr>
                            <th>#</th>
                            <th>Word</th>
                            <th>Transcription</th>
                            <th>Translation</th>
                        </tr>
                        </thead>
                        <tbody>
                        {list.length > 0 && list.map((item, index) =>
                            <VocPracTheoryItem word={item.word}
                                               transcription={item.transcription}
                                               translation={item.translation}
                                               key={index}
                                               num={index + 1}/>)}
                        </tbody>
                    </Table>
                </Container>
            </Container>
        </React.Fragment>


    )
};

export default VocPracTheory;
