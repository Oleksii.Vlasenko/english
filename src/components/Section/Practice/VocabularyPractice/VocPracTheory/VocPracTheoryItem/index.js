import React from "react";

const VocPracTheoryItem = ({num, word,transcription,translation}) => {

    return (
        <tr>
            <td className="text-center">{num}</td>
            <td>{word}</td>
            <td>[ {transcription} ]</td>
            <td>{translation}</td>
        </tr>
    )
};

export default VocPracTheoryItem;
