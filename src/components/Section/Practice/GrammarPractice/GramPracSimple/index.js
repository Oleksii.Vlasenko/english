import React, {useEffect, useState} from "react";
import {Button, Card, CardGroup, Col, Container, Row} from "react-bootstrap";
import GramPracSimpleItem from "./GramPracSimpleItem";
import {getGrammarTaskById} from '../../../../../services/grammarTasks.service';
import AlertEvent from "../../../../AlertEvent";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faTimesCircle} from "@fortawesome/free-solid-svg-icons";

const GramPracSimple = (props) => {

    const id = window.location.href.split('?id=')[1];
    // const id = "5ea1a9af79bf1339acf6c9c1";
    const [tasks, setTasks] = useState([]);
    const [index, setIndex] = useState(0);
    const [correctAnswers, setCorrectAnswers] = useState(0);
    const [result, setResult] = useState([]);
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    useEffect(() => {
        (async () => {
            try {
                await getGrammarTaskById(id)
                    .then(result => setTasks(shuffle(result.data).slice(0, 10)));
            } catch (e) {
                setAlertForm({message: e.message, variant: "danger"});
                setShow(true);
            }
        })();
    }, [id]);

    const checkAnswer = async (answer) => {
        setResult([...result, !!answer]);
        if (answer) {
            setCorrectAnswers(() => correctAnswers + 1);
        }
        setIndex(() => index + 1);
    };

    const shuffle = (arr) => {
        let index = arr.length;
        while (index) {
            index--;
            let randomIndex = Math.floor(Math.random() * index);
            [arr[index], arr[randomIndex]] = [arr[randomIndex], arr[index]];
        }
        return arr;
    };

    return (
        <React.Fragment>
            <Container fluid className="p-0 m-0 container-fluid-component task">
                <Container className="mt-5 pt-sm-0 pt-md-5 ">
                    {show &&
                    <AlertEvent variant={alertForm.variant}
                                hide={() => setShow(false)}
                                message={alertForm.message}/>}
                    {tasks.length > 0
                    && tasks.length > index
                    && <GramPracSimpleItem action={checkAnswer}
                                           item={tasks[index]}
                                           num={index + 1}
                                           length={tasks.length}
                    />}
                    {
                        tasks.length > 0
                        && tasks.length === index
                        && <Row>
                            <Col xs={12} lg={6} className="mx-auto">
                                <CardGroup>
                                    <Card className="rounded-2rem">
                                        <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up py-3">
                                            <span
                                                className="p-3 text-uppercase">Your mark is {Math.round(correctAnswers / tasks.length * 100)}</span>
                                            <Link to="/practice/grammar">
                                                <FontAwesomeIcon icon={faTimesCircle}
                                                                 className="text-light float-right mt-1 text-decoration-none"/>
                                            </Link>
                                        </Card.Header>
                                        <Card.Body>
                                            <Card.Text>
                                                <ol>
                                                    {tasks && tasks.map((item, index) =>
                                                        <li key={index}
                                                            className="text-dark-blue">
                                                            <span>{item.firstPart}</span>
                                                            <span
                                                                className={`${result[index] ? "text-info" : "text-danger"} mx-3 font-weight-bold`}>
                                                                {item.answer}
                                                            </span>
                                                            <span>{item.lastPart}</span>
                                                        </li>)}
                                                </ol>
                                            </Card.Text>
                                        </Card.Body>
                                        <Card.Footer>
                                            <Link to="/practice/grammar" className="w-100">
                                                <Button onClick={checkAnswer}
                                                        className="px-4 float-right bg-light-blue">
                                                    <FontAwesomeIcon icon={faCheck} className="text-light"/>
                                                </Button>
                                            </Link>
                                        </Card.Footer>
                                    </Card>
                                </CardGroup>
                            </Col>
                        </Row>
                    }
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default GramPracSimple;
