import React, {useState, useEffect} from "react";
import {Button, Card, CardGroup, Col, Form, FormControl, InputGroup, Row} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";


const GramPracSimpleItem = ({action, num, length, item}) => {

    const {firstPart, lastPart, answer, options} = item;

    const [answers, setAnswers] = useState([]);
    const [selectAnswer, setSelectAnswer] = useState(null);

    const [radio, setRadio] = useState(null);

    const shuffle = (arr) => {
        let index = arr.length;
        while (index) {
            index--;
            let randomIndex = Math.floor(Math.random() * index);
            [arr[index], arr[randomIndex]] = [arr[randomIndex], arr[index]];
        }
        return arr;
    };

    useEffect(() => {
        setAnswers(() => shuffle([...options, answer]));
        setRadio(null);
    }, [options, answer]);

    const changeRadio = (item) => {
        setRadio(item);
    };

    const changeHandler = (event) => {
        setSelectAnswer(event.target.id.trim());
    };

    const checkAnswer = () => {
        action(selectAnswer === answer.trim());
    };

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} lg={6} className="mx-auto">
                    <CardGroup>
                        <Card className="rounded-2rem">
                            <Card.Header className="font-weight-bold bg-light-blue rounded-2rem-up py-3">
                                <span className="p-3">{num} / {length}</span>
                                <Link to="/practice/grammar">
                                    <FontAwesomeIcon icon={faTimesCircle}
                                                     className="text-light float-right mt-1 text-decoration-none"/>
                                </Link>
                            </Card.Header>
                            <Card.Body>
                                <Card.Text className="pb-3">
                                    {firstPart} ________ {lastPart}
                                </Card.Text>
                                <Form onChange={changeHandler}>
                                    {answers
                                    && answers.map((item, index) =>
                                        <div onClick={() => changeRadio(index)}>
                                            <InputGroup key={index}
                                                        className="py-1">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Radio
                                                        checked={radio === index}
                                                        onClick={() => changeRadio(index)}
                                                        aria-label="Radio button for following text input"
                                                        name="formHorizontalRadios"
                                                        id={item}/>
                                                </InputGroup.Prepend>
                                                <FormControl aria-label="Text input with radio button"
                                                             disabled={true}
                                                             value={`${item}`}/>
                                            </InputGroup>
                                        </div>)}
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Button onClick={checkAnswer}
                                        className="px-4 float-right bg-light-blue"
                                >
                                    <FontAwesomeIcon icon={faArrowRight}
                                                     className="text-light"/>
                                </Button>
                            </Card.Footer>
                        </Card>
                    </CardGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
};

export default GramPracSimpleItem;
