import React, {useEffect, useState} from "react";
import {Accordion, Button, Card, Container, Modal} from "react-bootstrap";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";

const GrammarPractice = () => {
    const state = useSelector(state => state);
    const [themes, setThemes] = useState(state.grammarThemes);
    const [subthemes, setSubthemes] = useState(state.grammarSubthemes);
    const [subthemeId, setSubthemeId] = useState("");

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = (event) => {
        setSubthemeId(event.target.id);
        setShow(true);
    };

    useEffect(() => {
        setThemes(state.grammarThemes);
        setSubthemes(state.grammarSubthemes);
    }, [state]);

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component task"
            >
                <Container>
                    <h2 className="text-center font-weight-bold text-dark-blue py-4 font-dancing-script">
                        Grammar
                    </h2>
                    {themes
                    && <Accordion defaultActiveKey="0"
                                  className="pt-5">
                        {themes.map((item, index) =>
                            <Card id={item._id}
                                  key={index}
                                  className="mt-1 border-0 text-uppercase text-dark-blue pointer"
                                  style={{borderRadius: "1rem"}}>
                                <Accordion.Toggle as={Card.Header}
                                                  eventKey={index}
                                                  className="bg-light m-0 border-0">
                                    <h4 className="mb-0">{item.title}</h4>
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey={index}
                                                    className="text-capitalize">
                                    <ol>
                                        {
                                            subthemes &&
                                            subthemes
                                                .filter(subtheme => subtheme.theme === item._id)
                                                .map((elem, index) =>
                                                    <h4 className="my-3 ml-3"
                                                        key={index}>
                                                        <li onClick={handleShow}
                                                            id={elem._id}>{elem.title}</li>
                                                    </h4>)
                                        }
                                    </ol>
                                </Accordion.Collapse>
                            </Card>
                        )}
                    </Accordion>
                    }
                    <Modal show={show} onHide={handleClose} className="rounded-modal">
                        <Modal.Header closeButton className="text-light bg-light-blue text-uppercase rounded-2rem-up">
                            <Modal.Title>Choose the task</Modal.Title>
                        </Modal.Header>
                        <Modal.Footer className="d-flex justify-content-center rounded-2rem-down">
                            <div className="w-100 text-center">
                                <Link to={`/practice/grammar/simple/?id=${subthemeId}`}>
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-5 px-xs-4 bg-light-blue task-btn">
                                        Multiple choice
                                    </Button>
                                </Link>
                            </div>
                            <div className="w-100 text-center">
                                <Link to={`/practice/grammar/normal/?id=${subthemeId}`}>
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-5 px-xs-4 bg-light-blue task-btn">
                                        Scrambled sentences
                                    </Button>
                                </Link>
                            </div>
                            <div className="w-100 text-center">
                                <Link to={`/practice/grammar/hard/?id=${subthemeId}`}>
                                    <Button variant="dark" onClick={handleClose}
                                            className="w-100 px-sm-5 px-xs-4 bg-light-blue task-btn">
                                        Fill in the blank
                                    </Button>
                                </Link>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default GrammarPractice;
