import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {editGroupById} from "../../../../../store/actions/group.action";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StaffroomGroupEdit = (props) => {
    const {token} = useSelector(state => state.auth.user);
    const {title, groupId, groupKey, action} = props;
    const dispatch = useDispatch();

    const [form, setForm] = useState({
        title,
        groupKey
    });

    const changeHandler = (event) => {
        setForm({...form, [event.target.name]: event.target.value});
    };

    const submitHandler = async () => {
        editGroupById()(dispatch, token, {...form}, groupId);
        props.onHide();
        action(null);
    };

    return (
        <React.Fragment>
            <Modal
                show={props.show}
                onHide={props.onHide}
                size="sm"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="bg-light-blue text-light text-uppercase rounded-2rem-up">
                    <Modal.Title id="contained-modal-title-vcenter">
                        Edit group
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text"
                                          placeholder="Enter title"
                                          name="title"
                                          onChange={changeHandler}
                                          value={form.title}/>
                            <Form.Text className="text-muted">
                                This is new group name
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicGroupKey">
                            <Form.Label>Key</Form.Label>
                            <Form.Control type="text"
                                          placeholder="Enter key for students"
                                          name="groupKey"
                                          onChange={changeHandler}
                                          value={form.groupKey}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    <Button onClick={submitHandler} className="border-light bg-light-blue task-btn px-5">
                        <FontAwesomeIcon icon={faCheck}/>
                    </Button>
                    <Button onClick={props.onHide} className="border-light bg-light-blue task-btn px-5">
                        <FontAwesomeIcon icon={faTimes}/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
};

export default StaffroomGroupEdit;
