import React, {useEffect, useState} from "react";
import {Button, Collapse, OverlayTrigger, Table, Tooltip} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import StaffroomGroupItem from "./StaffroomGroupItem";
import ConfirmModal from "../../ConfirmModal";
import StaffroomGroupEdit from "../StaffroomGroupEdit";
import {removeGroupById} from '../../../../../store/actions/group.action';
import {removeStudentById} from "../../../../../store/actions/student.action";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faUsers, faEdit, faTrash, faChevronUp, faChevronDown} from '@fortawesome/free-solid-svg-icons';

const StaffroomGroup = ({controls, title, groupId, groupKey, action}) => {
    const {auth, students} = useSelector(state => state);
    const {studentsList} = students;
    const {token} = auth.user;
    const dispatch = useDispatch();


    const [open, setOpen] = useState(false);
    const [studentsGroup, setStudentsGroup] = useState(null);

    const [smShow, setSmShow] = useState(false);

    const [currentUser, setCurrentUser] = useState(null);

    const [editModal, setEditModal] = useState(false);

    useEffect(() => {
        const stud = studentsList.filter(item => item.groups.includes(groupId));
        setStudentsGroup(stud);
    },[students, groupId]);

    const removeUser = async (userId) => {
        removeStudentById()(dispatch, token, userId);
        setSmShow(false);
    };

    const removeGroup = async () => {
        if (studentsList.filter(item => `${item.groups}` === `${groupId}`).length === 0) {
            removeGroupById()(dispatch, token, groupId);
        }
    };

    return (
        <React.Fragment>

            <div className="mt-3">
                    <Button
                        onClick={() => setOpen(!open)}
                        aria-controls={`example-collapse-text-${controls}`}
                        aria-expanded={open}
                        style={{width: "10rem"}}
                        className="d-inline-block mr-4 mb-3 text-uppercase font-weight-bold text-light bg-light-blue task-btn"
                    >
                        <FontAwesomeIcon icon={faUsers} className="float-left"/>
                        {title}
                        <FontAwesomeIcon icon={open ? faChevronUp : faChevronDown} className="float-right pt-1"/>
                    </Button>

                <Button className="bg-light-blue task-btn w-4rem mb-3"
                        onClick={() => setEditModal(true)}
                >
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
                <Button className="bg-light-blue task-btn w-4rem ml-lb-5 ml-md-1 mb-3 float-right"
                        onClick={removeGroup}
                >
                    <FontAwesomeIcon icon={faTrash}/>
                </Button>
                <Collapse in={open}>
                    <div id="example-collapse-text">
                        {(!studentsGroup || studentsGroup.length === 0) && <div>Group is empty!</div>}

                        {studentsGroup && studentsGroup.length !== 0 &&
                        <Table striped
                               hover
                        >
                            <thead>
                            <tr className="text-center align-middle text-uppercase text-dark-blue bg-light-header font-weight-bold">
                                <th style={{width: '3rem'}}>#</th>
                                <th style={{width: '14rem'}}>Name</th>
                                <th style={{width: '10rem'}}>Created</th>
                                <th></th>
                                <th style={{width: '12rem'}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                studentsGroup.map((item, index) =>
                                    <StaffroomGroupItem key={index}
                                                        student={item}
                                                        num={index + 1}
                                                        show={setSmShow}
                                                        setCurrentUser={setCurrentUser}
                                                        title={title}
                                                        groupId={groupId}
                                    />)
                            }
                            </tbody>
                        </Table>
                        }
                    </div>
                </Collapse>
            </div>

            <ConfirmModal show={smShow}
                          onHide={setSmShow}
                          currentUser={currentUser}
                          remove={removeUser}
            />

            <StaffroomGroupEdit show={editModal}
                                onHide={() => setEditModal(false)}
                                groupId={groupId}
                                title={title}
                                groupKey={groupKey}
                                action={action}/>
        </React.Fragment>
    );
};

export default StaffroomGroup;
