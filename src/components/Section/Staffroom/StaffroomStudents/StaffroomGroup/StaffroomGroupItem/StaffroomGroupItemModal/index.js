import React, {useState} from "react";
import {Button, Col, Form, Modal, Row} from "react-bootstrap";
import {useSelector, useDispatch} from "react-redux";
import {editStudentById} from "../../../../../../../store/actions/student.action"
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import AlertEvent from "../../../../../../AlertEvent";

const StaffroomGroupItemModal = ({show, onHide, firstName, lastName, title, userId, groupId}) => {

    const {auth, groups} = useSelector(state => state);
    const {token} = auth.user;
    const {groupsList} = groups;
    const dispatch = useDispatch();

    const [form, setForm] = useState({
        firstName,
        lastName,
        groups: groupId

    });
    const [showAlert, setShowAlert] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    const getGroupIdByTitle = (title) => {
        for (const group of groupsList) {
            if (group.title === title) {
                return group._id;
            }
        }
    };

    const changeHandler = (event) => {
        if (event.target.name !== "groups") {
            setForm({...form, [event.target.name]: event.target.value});
        } else {
            setForm({...form, [event.target.name]: getGroupIdByTitle(event.target.value)});
        }
    };

    const editStudent = async () => {
        try {
            await editStudentById()(dispatch, token, form, userId);
            onHide();
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShowAlert(true);
        }
    };


    return (
        <React.Fragment>
            {showAlert &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShowAlert(false)}
                        message={alertForm.message} />}
            <Modal
                show={show}
                onHide={onHide}
                size="sm"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title id="contained-modal-title-vcenter" className="font-weight-bold">
                        Edit student
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group as={Row} controlId="formPlaintextPassword">
                            <Form.Label column sm="5">First Name</Form.Label>
                            <Col sm="7">
                                <Form.Control placeholder="First name"
                                              value={form.firstName}
                                              name="firstName"
                                              onChange={changeHandler}/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formPlaintextPassword">
                            <Form.Label column sm="5">Last Name</Form.Label>
                            <Col sm="7">
                                <Form.Control placeholder="Last name"
                                              value={form.lastName}
                                              name="lastName"
                                              onChange={changeHandler}/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formPlaintextPassword">
                            <Form.Label column sm="5">Group</Form.Label>
                            <Col sm="7">
                                <Form.Control as="select"
                                              name="groups"
                                              onChange={changeHandler}>
                                    {groupsList && groupsList.map((item, index) =>
                                        <option key={index}
                                                selected={title === item.title ? "selected" : ""}
                                        >
                                            {item.title}
                                        </option>
                                    )}
                                </Form.Control>
                            </Col>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    <Button onClick={editStudent}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue">
                        <FontAwesomeIcon icon={faCheck}/>
                    </Button>
                    <Button onClick={onHide}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue">
                        <FontAwesomeIcon icon={faTimes}/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
};

export default StaffroomGroupItemModal;
