import React from "react";
import {Button} from "react-bootstrap";
import StaffroomGroupItemModal from "./StaffroomGroupItemModal";


const StaffroomGroupItem = ({student, num, show, setCurrentUser, title, groupId}) => {
    const {firstName, lastName, created, _id} = student;

    const date = new Date(Date.parse(created));
    const dateToString = `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`;

    const [modalShow, setModalShow] = React.useState(false);

    return (
        <React.Fragment>
            <tr className="text-center align-middle bg-light">
                <td>{num}</td>
                <td>{firstName} {lastName}</td>
                <td>{dateToString}</td>
                <td></td>
                <td className="d-flex justify-content-between">
                    <Button className="w-4rem bg-light-blue task-btn mx-2"
                            size="sm"
                            onClick={() => {
                                show(true);
                                setCurrentUser({_id, lastName});
                            }}
                    >
                        Remove
                    </Button>
                    <Button size="sm"
                            className="w-4rem px-2 bg-light-blue task-btn mx-2"
                            onClick={() => setModalShow(true)}>
                        Edit
                    </Button>
                </td>
            </tr>
            <StaffroomGroupItemModal show={modalShow}
                                     onHide={() => setModalShow(false)}
                                     firstName={firstName}
                                     lastName={lastName}
                                     group={title}
                                     userId={_id}
                                     groupId={groupId}
                                     title={title}/>
        </React.Fragment>
    )
};

export default StaffroomGroupItem;
