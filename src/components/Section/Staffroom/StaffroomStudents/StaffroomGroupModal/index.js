import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {createNewGroup} from "../../../../../store/actions/group.action"
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import AlertEvent from "../../../../AlertEvent";

const StaffroomGroupModal = (props) => {
    const {token} = useSelector(state => state.auth.user);
    const dispatch = useDispatch();


    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    const [form, setForm] = useState({
        title: "",
        groupKey: ""
    });

    const changeHandler = (event) => {
        setForm({...form, [event.target.name]: event.target.value});
    };

    const submitHandler = async () => {
        try {
            await createNewGroup()(dispatch, token, form);
            props.onHide();
        } catch (e) {
            if (e.message === "Request failed with status code 400") {
                e.message = "Not unique group key! Please, try again!"
            }
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    return (
        <React.Fragment>
            {show &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShow(false)}
                        message={alertForm.message}/>}
            <Modal
                {...props}
                size="sm"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="bg-light-blue font-weight-bold text-light text-uppercase rounded-2rem-up">
                    <Modal.Title id="contained-modal-title-vcenter">
                        New group
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text"
                                          placeholder="Enter title"
                                          name="title"
                                          onChange={changeHandler}/>
                            <Form.Text className="text-muted">
                                This is new group name
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicGroupKey">
                            <Form.Label>Key</Form.Label>
                            <Form.Control type="text"
                                          placeholder="Enter key for students"
                                          name="groupKey"
                                          onChange={changeHandler}/>
                        </Form.Group>

                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    <Button className="bg-light-blue task-btn px-5 text-light"
                            onClick={submitHandler}>
                        <FontAwesomeIcon icon={faCheck}/>
                    </Button>
                    <Button className="bg-light-blue task-btn px-5 text-light"
                            onClick={props.onHide}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
};

export default StaffroomGroupModal;
