import React, {useEffect, useState} from "react";
import {Button, Container} from "react-bootstrap";
import StaffroomGroup from "./StaffroomGroup";
import StaffroomGroupModal from "./StaffroomGroupModal";
import {useDispatch, useSelector} from "react-redux";
import {saveStudents} from "../../../../store/actions/student.action";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';

const StaffroomUsers = () => {
    const {groups, auth, students} = useSelector(state => state);
    const {token} = auth.user;
    const {groupsList} = groups;
    const {studentsList} = students;

    const dispatch = useDispatch();

    useEffect(() => {
        if (groupsList && studentsList.length === 0) {
            saveStudents()(dispatch, token);
        }
    }, [dispatch, token, groupsList]);

    const [modalShow, setModalShow] = useState(false);

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component task"
            >
                <StaffroomGroupModal show={modalShow}
                                     onHide={() => setModalShow(false)}/>
                <Container className="p-0">
                    <h2 className="text-center font-weight-bold text-dark-blue py-4 font-dancing-script">
                        Your   groups
                    </h2>
                    <Button onClick={() => setModalShow(true)}
                            style={{width: "10rem"}}
                            className="mt-2 mb-4 text-light border-light bg-light-blue task-btn">
                        <FontAwesomeIcon icon={faPlus}/>
                    </Button>
                    {groupsList && groupsList.map((item, index) =>
                        <StaffroomGroup controls={index + 1}
                                        key={index}
                                        title={item.title}
                                        groupId={item._id}
                                        groupKey={item.groupKey}
                                        action={setModalShow}
                        />)
                    }
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default StaffroomUsers;
