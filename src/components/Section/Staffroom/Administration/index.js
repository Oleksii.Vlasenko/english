import React, {useEffect, useState} from "react";
import {
    Button,
    Container, FormControl,
    Table,
    ToggleButton,
    ToggleButtonGroup
} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import UserItem from "./UsersItem";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faSort, faSortDown, faSortUp} from "@fortawesome/free-solid-svg-icons";
import AdminPagination from "./AdminPagination";
import {getUsers} from "../../../../services/users.service";
import C from "../../../../store/consts";

const debounce = (fn, delay) => {
    let timer = null;
    return function (...args) {
        const context = this;
        timer && clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(context, args);
        }, delay);
    };
};

const Administration = () => {
    const {auth} = useSelector(state => state);
    const {token} = auth.user;
    const dispatch = useDispatch();

    const [sortBy, setSortBy] = useState("created");
    const [direction, setDirection] = useState(-1);
    const [searchstring, setSearchstring] = useState();
    const [roles, setRoles] = useState([4, 1, 2]);

    const [pages, setPages] = useState(1);
    const [activePage, setActivePage] = useState(1);

    const [userArr, setUserArr] = useState();

    const changeSearch = (event) => {
        setSearchstring(event.target.value);
    };

    const changeActivePage = (event) => {
        if (parseInt(event.target.innerText) !== activePage) {
            setActivePage(+event.target.innerText);
        }
    };

    const getUsersList = async () => {
        const pageLength = 10;
        let role = roles.length > 0 ? roles.reduce((a ,b) => a + b) : undefined;
        const headers = searchstring
            ? {sortBy, direction, role, searchstring, activePage, pageLength}
            : {sortBy, direction, role, activePage, pageLength};
        dispatch({type: C.LOADING_START});
        await getUsers({token, headers})
            .then(result => {
                const pageNum = result.data.pagesLength / pageLength + 1;
                setPages(() => pageNum);
                setUserArr(result.data.users);
            });
        dispatch({type: C.LOADING_END});
    };

    const sendRequest = debounce(getUsersList, 400);

    useEffect(() => {
        sendRequest();
    }, [activePage, sortBy, direction, roles]);

    const changeSort = (column) => {
        if (column !== sortBy) {
            setSortBy(column);
            setDirection(1);
        } else {
            setDirection(() => direction * -1);
        }
    };

    const sortIcon = (column) => {
        if (column !== sortBy) return faSort;
        return direction > 0 ? faSortUp : faSortDown;
    };

    return (
        <React.Fragment>
            <Container fluid
                       className="m-0 p-0 container-fluid-component task"
            >
                <Container className="p-0">
                    <h2 className="text-center font-italic font-weight-bold py-4 font-dancing-script text-dark-blue">
                        Users
                    </h2>
                    <div className="d-flex justify-content-between flex-wrap">
                        <ToggleButtonGroup type="checkbox"
                                           defaultValue={[4, 1, 2]}
                                           className="my-3"
                                           onChange={(event) => {
                                               setRoles(event);
                                               sendRequest();
                                           }}>
                            <ToggleButton value={4}
                                          className="toggle-btn"
                            >
                                Administrator
                            </ToggleButton>
                            <ToggleButton value={1}
                                          className="toggle-btn"
                            >
                                Teacher
                            </ToggleButton>
                            <ToggleButton value={2}
                                          className="toggle-btn"
                            >
                                Student
                            </ToggleButton>
                        </ToggleButtonGroup>
                        <div className="d-flex my-3">
                            <FormControl type="text"
                                         placeholder="Search"
                                         onChange={changeSearch}
                                         onKeyDown={(event) => {if (event.keyCode == 13) sendRequest()}}/>
                            <Button className="bg-light-blue" onClick={sendRequest}>
                                <FontAwesomeIcon icon={faSearch}/>
                            </Button>
                        </div>
                    </div>
                    <Table striped
                           hover
                           responsive
                           className="text-center bg-light">
                        <thead className="bg-light-header text-dark-blue text-uppercase">
                        <tr>
                            <th>#</th>
                            <th>Name
                                <FontAwesomeIcon icon={sortIcon("lastName")}
                                                 onClick={() => changeSort("lastName")}
                                                 className="ml-2"/>
                            </th>
                            <th>Role</th>
                            <th>Groups</th>
                            <th>Created
                                <FontAwesomeIcon icon={sortIcon("created")}
                                                 onClick={() => changeSort("created")}
                                                 className="ml-2"/>
                            </th>
                            <th>Active
                                <FontAwesomeIcon icon={sortIcon("active")}
                                                 onClick={() => changeSort("active")}
                                                 className="ml-2"/>
                            </th>
                            <th style={{width: "7rem"}}>Reset</th>
                            <th style={{width: "7rem"}}>Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        {userArr && userArr.map((item, index) =>
                            <UserItem key={index}
                                      num={index + 1}
                                      user={item}
                            />)
                        }
                        </tbody>
                    </Table>
                    <AdminPagination changeActivePage={changeActivePage}
                                     activePage={activePage}
                                     setActivePage={setActivePage}
                                     pages={pages}
                    />
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default Administration;
