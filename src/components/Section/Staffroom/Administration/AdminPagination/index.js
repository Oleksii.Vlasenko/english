import React from "react";
import Pagination from "react-bootstrap/Pagination";

const AdminPagination = ({changeActivePage, activePage, setActivePage, pages}) => {

    const prevItem = () => activePage > 1 && setActivePage(() => activePage - 1);
    const nextItem = () => activePage < pages && setActivePage(() => activePage + 1);

    const changeArr = () => {
        let arr = [];
        arr.push(<Pagination.Prev onClick={prevItem}/>);
        if (pages > 7) {
            if (activePage <= 4) {
                for (let i = 1; i <= 4; i++) {
                    arr.push(<Pagination.Item onClick={changeActivePage}
                                              active={activePage === i}>{i}</Pagination.Item>);
                }
                arr.push(<Pagination.Ellipsis/>);
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === pages - 1}>{pages - 1}</Pagination.Item>);
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === pages}>{pages}</Pagination.Item>);
            }
            if (activePage >= pages - 3) {
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === 1}>{1}</Pagination.Item>);
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === 2}>{2}</Pagination.Item>);
                arr.push(<Pagination.Ellipsis/>);
                for (let i = pages - 3; i <= pages; i++) {
                    arr.push(<Pagination.Item onClick={changeActivePage}
                                              active={activePage === i}>{i}</Pagination.Item>);
                }
            }
            if (activePage > 4 && activePage < pages - 3) {
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === 1}>{1}</Pagination.Item>);
                arr.push(<Pagination.Ellipsis/>);
                for (let i = 0; i < 3; i++) {
                    arr.push(<Pagination.Item onClick={changeActivePage}
                                              active={activePage === activePage - 1 + i}>
                        {activePage - 1 + i}
                    </Pagination.Item>);
                }
                arr.push(<Pagination.Ellipsis/>);
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === pages}>{pages}</Pagination.Item>);
            }
        } else {
            for (let i = 1; i <= pages; i++) {
                arr.push(<Pagination.Item onClick={changeActivePage}
                                          active={activePage === i}>{i}</Pagination.Item>);
            }
        }
        arr.push(<Pagination.Next onClick={nextItem}/>);
        return arr;
    };

    return (
        <React.Fragment>
            <Pagination className="justify-content-center">
                {changeArr()}
            </Pagination>
        </React.Fragment>
    );
};

export default AdminPagination;
