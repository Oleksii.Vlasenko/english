import React, {useEffect, useState} from "react";
import {Button} from "react-bootstrap";
import ConfirmModal from "../../ConfirmModal";
import {useDispatch, useSelector} from "react-redux";
import {faTrash, faUnlockAlt, faUserCheck, faUserLock} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {editStudentById, removeStudentById} from "../../../../../store/actions/student.action";
import {resetUserPassword} from '../../../../../services/users.service';
import AlertEvent from "../../../../AlertEvent";
import C from "../../../../../store/consts";

const UserItem = ({num, user}) => {
    const {token} = useSelector(state => state.auth.user);
    const {groupsList} = useSelector(state => state.groups);
    const {firstName, lastName, role, groups, created, _id: userId, active} = user;

    const dispatch = useDispatch();
    const [smShow, setSmShow] = useState(false);
    const [groupTitles, setGroupTitles] = useState([]);
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    useEffect(() => {
        if (groups && groups.length > 0) {
            let arr = groups.map(item =>
                groupsList.find(group =>
                    `${group._id}` === `${item}`));
            setGroupTitles(arr.map(item => item ? item.title : "rem group"));
        }
    }, [groups, groupsList]);

    const removeUser = async (userId) => {
        try {
            removeStudentById()(dispatch, token, userId);
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    const changeUserActive = (isActive) => {
        editStudentById()(dispatch, token, {active: !isActive}, userId);
    };

    const resetPassword = async () => {
        try {
            dispatch({type: C.LOADING_START});
            await resetUserPassword({token, id: userId});
            dispatch({type: C.LOADING_END});
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    const date = new Date(Date.parse(created));
    const dateToString = `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`; //.getMonth() < real month

    return (
        <React.Fragment>
            {show &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShow(false)}
                        message={alertForm.message} />}
            <tr className="text-dark-blue">
                <td>{num}</td>
                <td className="font-weight-bold">{lastName}, {firstName}</td>
                <td>{role}</td>
                <td>{groupTitles}</td>
                <td>{dateToString}</td>
                <td>
                    <Button size="sm"
                            className="bg-light-blue"
                            onClick={() => changeUserActive(active)}>
                        <FontAwesomeIcon icon={active ? faUserCheck : faUserLock}/>
                    </Button>
                </td>
                <td>
                    <Button size="sm"
                            className="bg-light-blue"
                            onClick={() => resetPassword()}
                    >
                        <FontAwesomeIcon icon={faUnlockAlt}/>
                    </Button>
                </td>
                <td>
                    <Button size="sm"
                            className="bg-light-blue"
                            onClick={() => setSmShow(true)}
                    >
                        <FontAwesomeIcon icon={faTrash}/>
                    </Button>
                </td>
            </tr>
            <ConfirmModal show={smShow}
                          onHide={setSmShow}
                          remove={removeUser}
                          currentUser={{lastName, _id: userId}}/>
        </React.Fragment>

    )
};

export default UserItem;
