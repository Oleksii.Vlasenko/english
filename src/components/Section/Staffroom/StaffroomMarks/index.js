import React, {useEffect} from "react";
import StaffroomMarksGroup from "./StaffroomMarksGroup";
import {useDispatch, useSelector} from "react-redux";
import {saveMarksByTaskGroup} from "../../../../store/actions/mark.action";
import {saveStudents} from "../../../../store/actions/student.action";
import {Container} from "react-bootstrap";

const StaffroomMarks = () => {
    const {auth, taskGroups, groups, students} = useSelector(state => state);
    const {token} = auth.user;
    const {taskGroupsList} = taskGroups;
    const {groupsList} = groups;
    const {studentsList} = students;
    const dispatch = useDispatch();

    useEffect(() => {
        if (taskGroupsList) {
            taskGroupsList.forEach(item =>
                saveMarksByTaskGroup()(dispatch, token, item._id));
        }
    }, [dispatch, token, taskGroupsList]);

    useEffect(() => {
        if (groupsList && studentsList.length === 0) {
            saveStudents()(dispatch, token);
        }
    }, [dispatch, token, groupsList]);

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component task"
            >
                <Container className="p-0">
                    <h2 className="text-center text-dark-blue font-dancing-script font-weight-bold py-4">
                        Marks
                    </h2>
                    {groupsList && groupsList.map((item, index) =>
                        <StaffroomMarksGroup controls={index + 1}
                                             key={index}
                                             title={item.title}
                                             groupId={item._id}
                                             groupKey={item.groupKey}
                        />)
                    }
                </Container>
            </Container>
        </React.Fragment>
    )
};

export default StaffroomMarks;
