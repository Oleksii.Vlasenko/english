import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {Button, Collapse, Container, OverlayTrigger, Table, Tooltip} from "react-bootstrap";
import StaffroomMarksGroupItem from "./StaffroomMarksGroupItem";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronDown, faChevronUp, faUsers} from "@fortawesome/free-solid-svg-icons";

const StaffroomMarksGroup = ({controls, title, groupId, groupKey}) => {
    const {students, taskGroups} = useSelector(state => state);
    const {studentsList} = students;
    const {taskGroupsList} = taskGroups;


    const [open, setOpen] = useState(false);
    const [studentsGroup, setStudentsGroup] = useState(null);
    const [currentTaskGroups, setCurrentTaskGroups] = useState(null);

    useEffect(() => {
        setStudentsGroup(studentsList.filter(item => item.groups.includes(groupId)));
    }, [studentsList, groupId]);

    const isFinish = (expDate) => new Date(expDate) < new Date();

    useEffect(() => {
        if (taskGroupsList.length > 0) {
            setCurrentTaskGroups(() => taskGroupsList.filter(item => item.groups.includes(groupId)));
        }
    }, [taskGroupsList, groupId]);

    return (
        <React.Fragment>

            <div className="mt-3">
                <Container>
                    <Button
                        onClick={() => setOpen(!open)}
                        aria-controls={`example-collapse-text-${controls}`}
                        aria-expanded={open}
                        className="mb-3 text-light bg-light-blue"
                        style={{width: "12rem"}}
                    >
                        <FontAwesomeIcon icon={faUsers}
                                         className="mr-1"
                        />
                        {title.toUpperCase()}
                        <FontAwesomeIcon icon={open ? faChevronUp : faChevronDown}
                                         className="float-right mt-1"
                        />
                    </Button>
                </Container>
                <Collapse in={open}>
                    <div id="example-collapse-text">
                        {(!studentsGroup || studentsGroup.length === 0) && <div>Group is empty!</div>}
                        {studentsGroup && studentsGroup.length !== 0
                        && <Table bordered
                                  hover
                                  responsive
                                  className="bg-light"
                        >
                            <thead className="bg-light-header text-center text-dark-blue">
                            <tr>
                                <th style={{width: '2rem'}}>#</th>
                                <th style={{width: '10rem'}}>Name</th>
                                <th></th>
                                {currentTaskGroups
                                && currentTaskGroups.map((item, index) =>
                                    <th nowrap
                                        key={index}
                                        style={{minWidth: "5rem", whiteSpace: "nowrap"}}
                                        className={`${isFinish(item.expDate) ? "text-danger" : "text-dark"}`}
                                    >
                                        {item.title}
                                    </th>)}
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                studentsGroup.map((item, index) =>
                                    <StaffroomMarksGroupItem key={index}
                                                             student={item}
                                                             num={index + 1}
                                                             title={title}
                                                             groupId={groupId}
                                                             taskGroups={currentTaskGroups}
                                    />)
                            }
                            </tbody>
                        </Table>
                        }
                    </div>
                </Collapse>
            </div>
        </React.Fragment>
    );
};

export default StaffroomMarksGroup;
