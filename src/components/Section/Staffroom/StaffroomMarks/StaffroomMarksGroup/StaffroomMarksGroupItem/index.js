import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {removeMarkById} from "../../../../../../store/actions/mark.action";

const StaffroomMarksGroupItem = ({student, num, taskGroups}) => {
    const {marks, auth} = useSelector(state => state);
    const {marksList} = marks;
    const {token} = auth.user;

    const {firstName, lastName, _id} = student;

    const dispatch = useDispatch();

    const createMarkToShow = (item) => {
        const mark = marksList.find(elem => elem.user === _id && elem.taskGroup === item._id);
        const expDate = new Date(item.expDate);
        const date = mark ? new Date(mark.updated) : 0;
        return <span className={`${expDate > date ? "text-dark-blue" : "text-danger"} font-weight-bold`}>
            {mark ? mark.value : "-"}
            {mark
            && <FontAwesomeIcon icon={faTimes}
                                className="ml-3 pointer text-dark-blue"
                                onClick={() => removeMark(item)}/>}
        </span>
    };

    const removeMark = (item) => {
        const mark = marksList.find(elem => elem.user === _id && elem.taskGroup === item._id);
        removeMarkById()(dispatch, token, mark._id);
    };

    return (
        <React.Fragment>
            <tr>
                <td className="text-center">{num}</td>
                <td style={{whiteSpace: "nowrap"}}>{firstName} {lastName}</td>
                <td></td>
                {taskGroups
                && taskGroups.map((item, index) =>
                    <td key={index} className={`text-center`}>
                        {createMarkToShow(item)}
                    </td>
                )}
                <td></td>
            </tr>
        </React.Fragment>
    )
};

export default StaffroomMarksGroupItem;
