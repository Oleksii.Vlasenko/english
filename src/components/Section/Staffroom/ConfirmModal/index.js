import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const ConfirmModal = ({show, onHide, remove, currentUser}) => {

    const [isDisabled, setIsDisabled] = useState(true);

    const confirmRemoving = () => {
        remove(currentUser._id);
        onHide();
    };

    const changeHandler = (event) => {
        event.target.value === currentUser.lastName ? setIsDisabled(false) : setIsDisabled(true);
    };

    return (
        <React.Fragment>
            <Modal
                size="sm"
                show={show}
                onHide={() => onHide(false)}
                aria-labelledby="example-modal-sizes-title-sm"
                className="rounded-modal"
            >
                <Modal.Header closeButton className="bg-light-blue text-light text-uppercase rounded-2rem-up">
                    <Modal.Title id="example-modal-sizes-title-sm">
                        Remove user
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control type="text" placeholder="Enter last name" onChange={changeHandler} />
                            <Form.Text className="text-muted">
                                Enter last name to confirm removing
                            </Form.Text>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="justify-content-between rounded-2rem-down bg-light-header">
                    <Button onClick={confirmRemoving}
                            disabled={isDisabled ? "disabled" : ""}
                            className="text-light px-4 bg-light-blue task-btn"
                    >
                        <FontAwesomeIcon icon={faCheck} className="mx-3 text-light" />
                    </Button>
                    <Button onClick={() => onHide(false)}
                            className="text-light px-4 bg-light-blue task-btn"
                    >
                        <FontAwesomeIcon icon={faTimes} className="mx-3 text-light" />
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
};

export default ConfirmModal;
