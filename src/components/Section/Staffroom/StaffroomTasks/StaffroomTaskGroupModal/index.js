import React, {useEffect, useState} from "react";
import {Button, Col, Form, Modal, Row, ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {addTaskGroup, editTaskGroupById} from "../../../../../store/actions/taskGroup.action";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StaffroomTaskGroupModal = ({show, onHide, groupsList, editItem}) => {
    const {token} = useSelector(state => state.auth.user);
    const dispatch = useDispatch();

    const [groups, setGroups] = useState(editItem ? editItem.groups : undefined);
    const [expDate, setExpDate] = useState(editItem ? editItem.expDate.split("T")[0] : undefined);
    const [form, setForm] = useState({
        title: editItem ? editItem.title : undefined,
        description: editItem ? editItem.description : undefined,
        taskType: editItem ? editItem.taskType : "grammar",
        difficulty: editItem ? editItem.difficulty : "easy",
        time: editItem ? editItem.time : undefined
    });

    const [border, setBorder] = useState({
        title: true,
        description: true,
        time: true,
        expDate: true,
        groups: true
    });

    const formValidate = () => {
        let valid = true;
        const checkBorder = {...border};
        if (!form.title) {
            checkBorder.title = false;
            valid = false;
        }
        if (!form.description) {
            checkBorder.description = false;
            valid = false;
        }
        if (form.time < 0) {
            checkBorder.time = false;
            valid = false;
        }
        if (!form.time) {
            checkBorder.time = false;
            valid = false;
        }
        if (expDate) {
            const date = new Date();
            const taskDate = new Date(expDate);
            if (date > taskDate) {
                checkBorder.expDate = false;
                valid = false;
            }
        }
        if (!expDate) {
            checkBorder.expDate = false;
            valid = false;
        }
        if (!groups) {
            checkBorder.groups = false;
            valid = false;
        }
        setBorder({...border, ...checkBorder});
        return valid;
    };

    const createTaskGroup = () => {
        if (formValidate()) {
            addTaskGroup()(dispatch, token, {...form, expDate, groups, active: false});
            onHide();
        }
    };

    const editTaskGroup = () => {
        editTaskGroupById()(dispatch, token, {...form, expDate, groups}, editItem._id);
        onHide();
    };

    const changeHandler = (event) => {
        setBorder({...border, [event.target.name]: true});
        setForm({...form, [event.target.name]: event.target.value});
    };

    const changeDateHandler = (event) => {
        setBorder({...border, expDate: true});
        setExpDate(event.target.value);
    };

    const changeGroupsHandler = (value) => {
        setBorder({...border, groups: true});
        setGroups(value);
    };

    return (
        <React.Fragment>
            <Modal
                show={show}
                onHide={onHide}
                size="md"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title id="contained-modal-title-vcenter">
                        Create new task
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form className="mx-auto">
                        <Form.Group as={Row} controlId="formPlaintextPassword">
                            <Form.Label column sm="4">
                                Title
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="text"
                                              placeholder="Enter task title"
                                              name="title"
                                              onChange={changeHandler}
                                              className={`${!border.title && "border-danger"}`}
                                              value={form.title}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}
                                    controlId="formPlaintextPassword">
                            <Form.Label column sm="4">
                                Description
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control as="textarea"
                                              rows="2"
                                              placeholder="Enter description"
                                              name="description"
                                              onChange={changeHandler}
                                              className={`${!border.description && "border-danger"}`}
                                              value={form.description}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}
                                    controlId="formPlaintextPassword">
                            <Form.Label column sm="4">
                                Expiration date
                            </Form.Label>
                            <Col sm="4">
                                <input
                                    type="date"
                                    onChange={changeDateHandler}
                                    name="expDate"
                                    style={{zIndex: "1"}}
                                    className={`${!border.expDate && "border-danger"} form-control p-0 text-center`}
                                    value={expDate}
                                />
                            </Col>
                            <Col sm="4">
                                <Form.Control as="select"
                                              custom
                                              name="taskType"
                                              onChange={changeHandler}
                                              value={form.taskType}
                                >
                                    <option>grammar</option>
                                    <option>vocabulary</option>
                                    <option>irregular verbs</option>
                                </Form.Control>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}>
                            <Form.Label column sm="4">
                                Difficulty
                            </Form.Label>
                            <Col sm={4}>
                                <Form.Control as="select"
                                              custom
                                              name="difficulty"
                                              onChange={changeHandler}
                                              value={form.difficulty}
                                >
                                    <option value="easy">{form.taskType === "grammar" ? "select" : "select"}</option>
                                    <option value="normal">{form.taskType === "grammar" ? "build" : "write"}</option>
                                    <option value="hard">{form.taskType === "grammar" ? "write" : "card"}</option>
                                </Form.Control>
                            </Col>
                            <Col sm="4">
                                <Form.Control type="number"
                                              placeholder="Time limit"
                                              name="time"
                                              onChange={changeHandler}
                                              className={`${!border.time && "border-danger"}`}
                                              value={form.time}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group controlId="formGridGroups"
                                    className="text-center">
                            <Form.Label column
                                        sm="12"
                                        className=" mb-3">
                                Groups
                            </Form.Label>
                            <ToggleButtonGroup type="checkbox"
                                               value={groups}
                                               onChange={changeGroupsHandler}>
                                {groupsList && groupsList.map((item, index) =>
                                    <ToggleButton variant="outline-secondary"
                                                  style={{zIndex: "0"}}
                                                  value={item._id}
                                                  key={index}
                                                  size="sm"
                                                  className={`${!border.groups && "border-danger"}`}
                                    >
                                        {item.title}
                                    </ToggleButton>)}
                            </ToggleButtonGroup>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between bg-light-header rounded-2rem-down">
                    {!editItem &&
                    <Button className="px-5 text-light text-uppercase font-weight-bold bg-light-blue"
                            onClick={createTaskGroup}
                    >
                        <FontAwesomeIcon icon={faCheck} className="mx-3 text-light"/>
                    </Button>}
                    {editItem &&
                    <Button className="px-5 text-light text-uppercase font-weight-bold bg-light-blue"
                            onClick={editTaskGroup}
                    >
                        <FontAwesomeIcon icon={faCheck} className="mx-3 text-light"/>
                    </Button>}
                    <Button onClick={onHide}
                            className="px-5 text-light text-uppercase font-weight-bold bg-light-blue"
                    >
                        <FontAwesomeIcon icon={faTimes} className="mx-3 text-light"/>
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
};

export default StaffroomTaskGroupModal;
