import React, {useState} from "react";
import {Button, Container} from "react-bootstrap";
import {useSelector} from "react-redux";
import StaffroomTasksGroups from "./StaffroomTaskGroups";
import StaffroomTaskGroupModal from "./StaffroomTaskGroupModal";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';

const StaffroomTasks = () => {

    const {groups, taskGroups} = useSelector(state => state);
    const {groupsList} = groups;
    const {taskGroupsList} = taskGroups;


    const [modalShow, setModalShow] = useState(false);

    return (
        <React.Fragment>
            <Container fluid
                       className="p-0 m-0 container-fluid-component task"
            >
                <Container>
                    <h2 className="text-center font-weight-bold text-dark-blue py-4 font-dancing-script">
                        Your tasks
                    </h2>
                    <Button className="mt-2 mb-4 text-light border-light bg-light-blue task-btn"
                            onClick={() => setModalShow(true)}
                            style={{width: "10rem"}}
                    >
                        <FontAwesomeIcon icon={faPlus}/>
                    </Button>
                    {groupsList && groupsList.map((item, index) =>
                        <StaffroomTasksGroups controls={index + 1}
                                              key={index}
                                              title={item.title}
                                              groupId={item._id}
                                              groupKey={item.groupKey}
                                              taskGroups={taskGroupsList}
                        />)
                    }
                </Container>
                <StaffroomTaskGroupModal show={modalShow}
                                         onHide={() => setModalShow(false)}
                                         groupsList={groupsList}/>
            </Container>
        </React.Fragment>
    )
};

export default StaffroomTasks;
