import React, {useState} from "react";
import {Button, Collapse} from "react-bootstrap";
import StaffroomTaskGroup from "./StaffroomTaskGroup";
import {faUsers, faChevronDown, faChevronUp} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StaffroomTasksGroups = ({title, controls, taskGroups, groupId, action}) => {

    const [open, setOpen] = useState(false);

    return (
        <React.Fragment>
            <div>
                <Button
                    onClick={() => setOpen(!open)}
                    aria-controls={`example-collapse-text-${controls}`}
                    aria-expanded={open}
                    style={{width: "10rem"}}
                    className={`d-inline-block mr-5 mb-3 text-uppercase font-weight-bold text-light border-light bg-light-blue task-btn`}
                >
                    <FontAwesomeIcon icon={faUsers}
                                     className="float-left"/>
                    {title}
                    <FontAwesomeIcon icon={open ? faChevronUp : faChevronDown}
                                     className="float-right pt-1"/>
                </Button>

                <Collapse in={open}>
                    <div className="row row-cols-xl-3 row-cols-md-2 row-cols-sm-1">
                        {
                            taskGroups && taskGroups
                                .filter(item => item.groups ? item.groups.includes(groupId) : false)
                                .map((item, index) =>
                                    <StaffroomTaskGroup key={index}
                                                        item={item}
                                                        action={action}/>
                                )
                        }
                    </div>

                </Collapse>
            </div>
        </React.Fragment>
    )
};

export default StaffroomTasksGroups;
