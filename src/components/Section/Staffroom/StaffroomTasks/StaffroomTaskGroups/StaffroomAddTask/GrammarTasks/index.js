import React, {useEffect, useState} from "react";
import {Button, Table} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {editGrammarTaskById, removeGrammarTaskById} from "../../../../../../../store/actions/grammarTask.action";
import {faMinusSquare, faPlusSquare, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Spinners from "../../../../../Spinners";
import {getGrammarTaskById} from "../../../../../../../services/grammarTasks.service";
import C from "../../../../../../../store/consts";

const GrammarTasks = ({taskGroupId, active}) => {

    const {grammarTasks, auth, grammarSubthemes} = useSelector(state => state);
    const {grammarTasksList, isLoading} = grammarTasks;
    const {token} = auth.user;

    const [tasks, setTasks] = useState(null);

    const id = window.location.href.split('?id=')[1];
    const dispatch = useDispatch();

    const getSubthemeTasks = async (subthemeId) => {
        dispatch({type: C.LOADING_START});
        await getGrammarTaskById(subthemeId)
            .then(result => setTasks(result.data.filter(item => !item.taskGroups.includes(id))));
        dispatch({type: C.LOADING_END});
    };

    useEffect(() => {
        if (active) {
            setTasks(grammarTasksList.filter(item => item.taskGroups.includes(taskGroupId)))
        }
    }, [taskGroupId, active, grammarSubthemes, grammarTasksList]);

    useEffect(() => {
        if (!active) {
            if (taskGroupId !== -1) {
                getSubthemeTasks(taskGroupId);
            } else {
                const arr = grammarTasksList.filter(item => !item.taskGroups.includes(taskGroupId));
                setTasks(arr.filter(item => !item.taskGroups.includes(id)));
            }
        }
    }, [taskGroupId, active, grammarSubthemes, grammarTasksList]);

    const editTaskGroups = async (item, isAddBtn) => {
        const body = isAddBtn
            ? {taskGroups: [...item.taskGroups, id]}
            : {taskGroups: item.taskGroups.filter(item => item !== id)};
        await editGrammarTaskById()(dispatch, token, body, item._id);
    };

    const removeTaskGroup = async (taskId) => {
        await removeGrammarTaskById()(dispatch, token, taskId);
    };

    return (
        <React.Fragment>
            <Table bordered
                   striped
                   hover
                   responsive="lg"
                   className="mt-4 align-middle bg-light"
            >
                <thead>
                <tr className="text-center text-uppercase text-dark-blue font-weight-bold bg-light-header">
                    <th>#</th>
                    <th style={{width: "80%"}}>Sentence</th>
                    <th></th>
                    <th style={{minWidth: "3rem"}}></th>
                </tr>
                </thead>
                <tbody>
                {
                    tasks && tasks.map((item, index) =>
                        <tr key={index}>
                            <td className="text-center">
                                {index + 1}
                            </td>
                            <td>
                                {item.firstPart} {item.answer} {item.lastPart}
                            </td>
                            <td className="text-center">
                                {active
                                    ? <Button size="sm"
                                              className="px-4 bg-light-blue"
                                              onClick={() => editTaskGroups(item, false)}>
                                        <FontAwesomeIcon icon={faMinusSquare}/>
                                    </Button>
                                    : <Button size="sm"
                                              className="px-4 bg-light-blue"
                                              onClick={() => editTaskGroups(item, true)}>
                                        <FontAwesomeIcon icon={faPlusSquare}/>
                                    </Button>}
                            </td>
                            <td className="text-center">
                                {item.user === auth.user.id
                                && <Button size="sm"
                                           onClick={() => removeTaskGroup(item._id)}
                                           className="bg-light-blue">
                                    <FontAwesomeIcon icon={faTrash}/>
                                </Button>}
                            </td>
                        </tr>)
                }
                </tbody>
            </Table>
            {isLoading && active && <Spinners />}
        </React.Fragment>
    )
};

export default GrammarTasks;
