import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Container, Dropdown, DropdownButton} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import GrammarTasks from "./GrammarTasks";
import AddGrammarTask from "./AddGrammarTask";
import {saveGrammarTasks} from "../../../../../../store/actions/grammarTask.action";
import {saveVocabularyTasks} from "../../../../../../store/actions/vocabularyTask.action";
import VocabularyTasks from "./VocabularyTasks";
import AddVocabularyTask from "./AddVocabularyTask";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StaffroomAddTask = () => {

    const {taskGroups, auth, grammarThemes, grammarSubthemes} = useSelector(state => state);
    const {token} = auth.user;
    const {taskGroupsList} = taskGroups;

    const dispatch = useDispatch();

    const id = window.location.href.split('?id=')[1];
    const [taskType, setTaskType] = useState(null);

    const [taskGroupId, setTaskGroupId] = useState(-1);

    const [show, setShow] = useState(false);

    useEffect(() => {
        const taskGroup = taskGroupsList.find(item => item._id === id);
        if (taskGroup) {
            setTaskType(taskGroup.taskType);

            switch (taskGroup.taskType) {
                case "grammar":
                    saveGrammarTasks()(dispatch, token);
                    break;
                case "vocabulary":
                    saveVocabularyTasks()(dispatch, token, id);
                    break;
                default:
                    break;
            }
        }
    }, [taskGroupsList, setTaskType, id, dispatch, token]);

    return (
        <React.Fragment>
            <Container fluid
                       className="m-0 p-0 container-fluid-component task"
            >
                <Container className="mt-5">
                    <Button onClick={() => setShow(true)}
                            className="bg-light-blue"
                    >
                        <FontAwesomeIcon icon={faPlus} className="mx-5"/>
                    </Button>

                    {/*GRAMMAR TASKS IN TASKGROUP*/}
                    {taskType === "grammar" &&
                    <h2 className="mb-5 text-uppercase text-dark-blue text-center font-weight-bold">
                        Your task
                    </h2>}
                    {taskType === "grammar" && <GrammarTasks taskGroupId={id}
                                                             active={true}/>}


                    {/*VOCABULARY TASKS IN TASKGROUP*/}
                    {taskType === "vocabulary" && <VocabularyTasks taskGroupId={id}
                                                                   active={true}/>}

                    {/*GRAMMAR THEMES MENU*/}
                    {taskType === "grammar" &&
                    <div className="text-center mt-5">
                        <h2 className="mb-5 text-uppercase text-dark-blue text-center font-weight-bold">
                            Possible exercises:
                        </h2>
                        <div className="d-flex justify-content-between flex-wrap">
                            <Button className="mx-1 bg-light-blue"
                                    size="sm"
                                    onClick={() => setTaskGroupId(-1)}
                            >
                                My tasks
                            </Button>
                            {grammarThemes &&
                            grammarThemes.map((item, index) => <DropdownButton
                                as={ButtonGroup}
                                id={`dropdown-variants-primary`}
                                className="mx-1 bg-light-blue"
                                key={index}
                                title={item.title}
                                size="sm"
                            >
                                {grammarSubthemes &&
                                grammarSubthemes
                                    .filter(subItem => subItem.theme === item._id)
                                    .map((subItem, index) =>
                                        <Dropdown.Item eventKey={subItem.title}
                                                       key={index}
                                                       onClick={() => setTaskGroupId(subItem._id)}
                                        >
                                            {subItem.title}
                                        </Dropdown.Item>)
                                }
                            </DropdownButton>)
                            }
                        </div>
                    </div>
                    }

                    {/*GRAMMAR TASKS TABLE*/}
                    {taskType === "grammar" && <GrammarTasks taskGroupId={taskGroupId}/>}

                    {/*VOCABULARY TASKS TABLE*/}
                    {taskType === "vocabulary" && <VocabularyTasks taskGroupId={id}/>}

                    {/*GRAMMAR TASKS MODAL*/}
                    {taskType === "grammar" && <AddGrammarTask show={show}
                                                               setShow={setShow}
                                                               taskGroupId={id}/>}
                    {/*VOCABULARY TASKS MODAL*/}
                    {taskType === "vocabulary" && <AddVocabularyTask show={show}
                                                                     setShow={setShow}
                                                                     taskGroupId={id}/>}


                </Container>
            </Container>
        </React.Fragment>
    )
};

export default StaffroomAddTask;
