import React, {useState} from "react";
import {Button, Col, Form, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {addVocabularyTask} from "../../../../../../../store/actions/vocabularyTask.action";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const AddVocabularyTask = ({show, setShow, taskGroupId}) => {
    const {token} = useSelector(state => state.auth.user);

    const dispatch = useDispatch();

    const [form, setForm] = useState({
        word: null,
        transcription: null,
        translation: null,
        description: null
    });

    const changeHandler = (event) => {
        setForm({...form, [event.target.name]: event.target.value})
    };

    const sendRequest = async () => {
        const body = {
            ...form,
            taskGroups: taskGroupId
        };
        addVocabularyTask()(dispatch, token, body);
        setShow(false);
    };

    return (
        <React.Fragment>
            <Modal
                size="lg"
                show={show}
                onHide={() => setShow(false)}
                aria-labelledby="example-modal-sizes-title-lg"
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title id="example-modal-sizes-title-lg">
                        Add new task
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridWord">
                                <Form.Label>Word</Form.Label>
                                <Form.Control type="text" placeholder="Enter word" name="word"
                                              onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridTranscription">
                                <Form.Label>Transcription</Form.Label>
                                <Form.Control type="text" placeholder="Enter transcription" name="transcription"
                                              onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridTranslation">
                                <Form.Label>Translation</Form.Label>
                                <Form.Control type="text" placeholder="Enter translation" name="translation"
                                              onChange={changeHandler}/>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row className="mb-5">
                            <Form.Group controlId="formGridDescription" as={Col} sm={4}>
                                <Form.Label>Description</Form.Label>
                                <Form.Control placeholder="Enter description" name="description"
                                              onChange={changeHandler}/>
                            </Form.Group>
                        </Form.Row>
                        <Button className="px-5 text-light text-uppercase font-weight-bold bg-light-blue ml-5 float-right"
                                onClick={() => setShow(false)}>
                            <FontAwesomeIcon icon={faTimes} className="mx-2 text-light" />
                        </Button>
                        <Button className="px-5 text-light text-uppercase font-weight-bold bg-light-blue mr-5 float-right"
                                onClick={sendRequest}>
                            <FontAwesomeIcon icon={faCheck} className="mx-2 text-light" />
                        </Button>
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
};

export default AddVocabularyTask;
