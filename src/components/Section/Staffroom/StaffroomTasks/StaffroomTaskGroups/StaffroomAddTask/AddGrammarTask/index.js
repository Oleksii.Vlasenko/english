import React, {useState} from "react";
import {Button, Col, Form, Modal} from "react-bootstrap";
import {addGrammarTask} from '../../../../../../../store/actions/grammarTask.action'
import {useDispatch, useSelector} from "react-redux";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const AddGrammarTask = ({show, setShow, taskGroupId}) => {

    const {token} = useSelector(state => state.auth.user);
    const dispatch = useDispatch();

    const [form, setForm] = useState({
        firstPart: "",
        lastPart: "",
        answer: "",
        translation: "",
        infinitive: ""
    });
    const [options, setOptions] = useState([]);

    const [isPublic, setIsPublic] = useState(false);

    const sendRequest = () => {
        const body = {
            ...form,
            isPublic,
            taskGroups: taskGroupId,
            options
        };
        addGrammarTask()(dispatch, token, body);
        setShow(false);
    };

    const changeHandler = (event) => {
        setForm({...form, [event.target.name]: event.target.value})
    };

    const changeSwitch = (event) => {
        setIsPublic(event.target.checked);
    };

    const changeOptions = (event) => {
        let result = options;
        result[event.target.name] = event.target.value;
        setOptions(result);
    };

    return (
        <React.Fragment>
            <Modal
                size="lg"
                show={show}
                onHide={() => setShow(false)}
                aria-labelledby="example-modal-sizes-title-lg"
                className="rounded-modal"
            >
                <Modal.Header closeButton
                              className="rounded-2rem-up text-uppercase text-light font-weight-bold bg-light-blue">
                    <Modal.Title id="example-modal-sizes-title-lg">
                        Add new task
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridFirstPart">
                                <Form.Label>First part</Form.Label>
                                <Form.Control type="text" placeholder="Enter first part" name="firstPart"
                                              onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group as={Col} sm={2} controlId="formGridAnswer">
                                <Form.Label>Answer</Form.Label>
                                <Form.Control type="text" placeholder="Answer" name="answer" onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridLastPart">
                                <Form.Label>Last Part</Form.Label>
                                <Form.Control type="text" placeholder="Last Part" name="lastPart"
                                              onChange={changeHandler}/>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group controlId="formGridOption1" as={Col} className="mx-auto">
                                <Form.Label>Option 1</Form.Label>
                                <Form.Control placeholder="Option" name="0" onChange={changeOptions}/>
                            </Form.Group>

                            <Form.Group controlId="formGridOption2" as={Col} className="mx-auto">
                                <Form.Label>Option 2</Form.Label>
                                <Form.Control placeholder="Option" name="1" onChange={changeOptions}/>
                            </Form.Group>

                            <Form.Group controlId="formGridOption3" as={Col} className="mx-auto">
                                <Form.Label>Option 3</Form.Label>
                                <Form.Control placeholder="Option" name="2" onChange={changeOptions}/>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridTranslation">
                                <Form.Label>Translation</Form.Label>
                                <Form.Control type="text" placeholder="Enter translation" name="translation"
                                              onChange={changeHandler}/>
                            </Form.Group>

                            <Form.Group controlId="formGridInfinitive" as={Col} sm={2} className="mx-auto">
                                <Form.Label>Infinitive</Form.Label>
                                <Form.Control placeholder="Infinitive" type="text" name="infinitive" onChange={changeHandler}/>
                            </Form.Group>
                        </Form.Row>

                        <Form.Group id="formGridCheckbox">
                            <Form.Check
                                type="switch"
                                id="custom-switch"
                                label="Public"
                                name="isPublic"
                                onChange={changeSwitch}
                            />
                        </Form.Group>

                        <Button onClick={() => setShow(false)}
                                className="px-5 text-light text-uppercase font-weight-bold bg-light-blue ml-5 float-right">
                            <FontAwesomeIcon icon={faTimes} className="mx-2 text-light"/>
                        </Button>
                        <Button onClick={sendRequest}
                                className="px-5 text-light text-uppercase font-weight-bold bg-light-blue mr-5 float-right">
                            <FontAwesomeIcon icon={faCheck} className="mx-2 text-light"/>
                        </Button>
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
};

export default AddGrammarTask;
