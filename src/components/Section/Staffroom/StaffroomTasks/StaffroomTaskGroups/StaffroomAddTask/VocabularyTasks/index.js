import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    editVocabularyTaskById,
    removeVocabularyTaskById
} from "../../../../../../../store/actions/vocabularyTask.action";
import {Button, Form, FormControl, Navbar, Table} from "react-bootstrap";
import {faMinusSquare, faPlusSquare, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {getVocabulary} from "../../../../../../../services/vocabularyTasks.service";
import AlertEvent from "../../../../../../AlertEvent";
import C from "../../../../../../../store/consts";

const debounce = (fn, delay) => {
    let timer = null;
    return function (...args) {
        const context = this;
        timer && clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(context, args);
        }, delay);
    };
};

const VocabularyTasks = ({taskGroupId, active}) => {

    const {vocabularyTasks, auth} = useSelector(state => state);
    const {token} = auth.user;
    const {vocabularyTasksList} = vocabularyTasks;

    const dispatch = useDispatch();

    const getVoc = async (value) => {
        dispatch({type: C.LOADING_START});
        await getVocabulary(token, taskGroupId, value)
            .then(result => setTasks(result.data));
        dispatch({type: C.LOADING_END});
    };

    const sendReq = debounce(getVoc, 400);

    const [tasks, setTasks] = useState(null);
    const [show, setShow] = useState(false);
    const [alertForm, setAlertForm] = useState({});

    useEffect(() => {
        if (vocabularyTasksList && active) {
            setTasks(vocabularyTasksList.filter(item => item.taskGroups.includes(taskGroupId)));
        }
    }, [vocabularyTasks, taskGroupId, active, vocabularyTasksList]);

    const editTaskGroups = (item, isAddBtn) => {
        const body = isAddBtn
            ? {taskGroups: [...item.taskGroups, taskGroupId]}
            : {taskGroups: item.taskGroups.filter(item => item !== taskGroupId)};
        editVocabularyTaskById()(dispatch, token, body, item._id);
        if (isAddBtn) {
            setTasks(() => tasks.filter(task => task._id !== item._id));
        }
    };

    const removeTaskGroup = (taskId) => {
        removeVocabularyTaskById()(dispatch, token, taskId);
    };

    const changeHandler = async (event) => {
        try {
            sendReq(event.target.value);
        } catch (e) {
            setAlertForm({message: e.message, variant: "danger"});
            setShow(true);
        }
    };

    return (
        <React.Fragment>
            {show &&
            <AlertEvent variant={alertForm.variant}
                        hide={() => setShow(false)}
                        message={alertForm.message} />}
            {!active &&
            <Navbar variant="dark"
                    className="bg-middle-blue">
                <Form inline>
                    <FormControl type="text"
                                 placeholder="Search"
                                 className="mr-sm-2"
                                 onChange={changeHandler}
                    />
                    <Form.Label className="text-white">Start entering the word</Form.Label>
                </Form>
            </Navbar>
            }
            <Table bordered
                   striped
                   hover
                   responsive="lg"
                   className="mt-4 text-center align-middle bg-light"
            >
                <thead>
                <tr className="text-center text-uppercase text-dark-blue font-weight-bold bg-light-header">
                    <th>#</th>
                    <th>Word</th>
                    <th>Transcription</th>
                    <th>Translation</th>
                    <th>-</th>
                    <th style={{minWidth: "3rem"}}></th>
                </tr>
                </thead>
                <tbody>
                {
                    tasks && tasks.map((item, index) =>
                        <tr key={index}>
                            <td>
                                {index + 1}
                            </td>
                            <td>
                                {item.word}
                            </td>
                            <td>
                                {item.transcription}
                            </td>
                            <td>
                                {item.translation}
                            </td>
                            <td className="text-center">
                                {active
                                    ? <Button size="sm"
                                              className="px-4 bg-light-blue"
                                              onClick={() => editTaskGroups(item, false)}
                                    >
                                        <FontAwesomeIcon icon={faMinusSquare}/>
                                    </Button>
                                    : <Button size="sm"
                                              className="px-4 bg-light-blue"
                                              onClick={() => editTaskGroups(item, true)}
                                    >
                                        <FontAwesomeIcon icon={faPlusSquare}/>
                                    </Button>}
                            </td>
                            <td>
                                {item.user === auth.user.id
                                && <Button size="sm"
                                           onClick={() => removeTaskGroup(item._id)}
                                           className="bg-light-blue">
                                    <FontAwesomeIcon icon={faTrash}/>
                                </Button>}
                            </td>
                        </tr>)
                }
                </tbody>
            </Table>
        </React.Fragment>
    )
};

export default VocabularyTasks;
