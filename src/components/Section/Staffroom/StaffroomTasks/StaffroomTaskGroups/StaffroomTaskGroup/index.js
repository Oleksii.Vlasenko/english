import React, {useState} from "react";
import {Badge, Button, Card, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {editTaskGroupById, removeTaskGroupById} from "../../../../../../store/actions/taskGroup.action";
import StaffroomTaskGroupModal from "../../StaffroomTaskGroupModal";
import {Link} from "react-router-dom";
import {faEdit, faTrashAlt, faTasks} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const StaffroomTaskGroup = ({item}) => {
    const {auth, groups} = useSelector(state => state);
    const {groupsList} = groups;

    const {token} = auth.user;
    const dispatch = useDispatch();

    const [modalShow, setModalShow] = useState(false);

    const removeTaskGroup = () => {
        removeTaskGroupById()(dispatch, token, item._id);
    };

    const changeActive = (event) => {
        editTaskGroupById()(dispatch, token, {active: event.target.checked}, item._id);
    };

    const convertDate = (expDate) => {
        const date = new Date(Date.parse(expDate));
        return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`;
    };

    return (
        <React.Fragment>
            <div className="col my-3 d-flex justify-content-center">
                <Card className="shadow-lg text-dark-blue rounded-2rem w-20rem">
                    <Card.Header className="d-block bg-light-header rounded-2rem-up pr-0">
                        <Card.Title className="m-0">
                            {item.title}
                            <Form.Check
                                onChange={changeActive}
                                checked={item.active}
                                className="float-right"
                                type="switch"
                                id={`${item._id}kl2203`}
                                label=""
                            />
                        </Card.Title>
                    </Card.Header>
                    <Card.Body className="d-block"
                               style={{height: '7rem'}}>
                        <Badge variant="secondary"
                               className="float-right">
                            {convertDate(item.expDate)}
                        </Badge>
                        <Card.Text>
                            {item.description}
                        </Card.Text>
                    </Card.Body>
                    <Card.Footer className="d-block">
                        <Link to={`/class/tasks/newtask?id=${item._id}`} className="mr-auto">
                            <Button className="w-4rem bg-light-blue task-btn border-light">
                                <FontAwesomeIcon icon={faTasks}/>
                            </Button>
                        </Link>
                        <Button className="border-light bg-light-blue task-btn w-4rem float-right"
                                onClick={() => setModalShow(true)}>
                            <FontAwesomeIcon icon={faEdit}/>
                        </Button>
                        <Button className="border-light bg-light-blue task-btn mr-4 w-4rem float-right"
                                onClick={removeTaskGroup}>
                            <FontAwesomeIcon icon={faTrashAlt}/>
                        </Button>
                    </Card.Footer>
                </Card>
            </div>
            <StaffroomTaskGroupModal show={modalShow}
                                     onHide={() => setModalShow(false)}
                                     groupsList={groupsList}
                                     editItem={item}/>

        </React.Fragment>
    )
};

export default StaffroomTaskGroup;
